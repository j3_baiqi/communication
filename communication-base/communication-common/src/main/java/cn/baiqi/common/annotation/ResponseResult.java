package cn.baiqi.common.annotation;

import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;

/**
 * @author J3-白起
 * @package cn.baiqi.common.annotation
 * @createTime 2021/11/24 - 18:33
 * @description 统一结果
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@RestController
public @interface ResponseResult {
    boolean ignore() default false;
}
