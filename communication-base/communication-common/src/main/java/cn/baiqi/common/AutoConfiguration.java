package cn.baiqi.common;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("cn.baiqi.common")
public class AutoConfiguration {


}
