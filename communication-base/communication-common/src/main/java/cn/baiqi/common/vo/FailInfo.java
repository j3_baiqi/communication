package cn.baiqi.common.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * @author J3-白起
 * @package cn.baiqi.common.vo
 * @createTime 2021/11/24 - 18:41
 * @description 失败结果
 */
@Builder
@Getter
@ToString
public class FailInfo extends ResultInfo {

    protected static final Integer DEFAULT_CODE = 50000;
    protected static final String DEFAULT_MESSAGE = "操作失败";

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String exception;

    protected FailInfo(String exception) {
        super(false, DEFAULT_CODE, DEFAULT_MESSAGE);
        this.exception = exception;
    }

}
