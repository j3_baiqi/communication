package cn.baiqi.common.exception;

/**
 * @author J3-白起
 * @package cn.baiqi.common.exception
 * @createTime 2021/11/25 - 8:40
 * @description
 */
public class SysException extends RuntimeException {
    public SysException() {
    }
    public SysException(String message, Object... args) {
        super(String.format(message, args));
    }

    public SysException(String message, Throwable cause, Object... args) {
        super(String.format(message, args), cause);
    }

    public SysException(Throwable cause) {
        super(cause);
    }

}
