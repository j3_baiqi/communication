package cn.baiqi.common.constant;

/**
 * @author J3-白起
 * @package cn.baiqi.common.constant
 * @createTime 2021/12/2 - 14:53
 * @description
 */
public class BotFunctionConstant {

    /**
     * 打招呼关键词
     */
    public static final String SAY_HELLO = "打招呼";
    public static final String PRIVATE_DEFAULT_RETURN = "私聊默认返回";
    public static final String GROUP_DEFAULT_RETURN = "群聊默认返回";
    public static final String NEW_PEOPLE_ADD_GROUP = "新人入群";
    public static final String EMPTY = "呜呜呜~ 我还任何功能呢！";

}
