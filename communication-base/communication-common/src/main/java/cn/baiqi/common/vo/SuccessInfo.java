package cn.baiqi.common.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * @author J3-白起
 * @package cn.baiqi.common.vo
 * @createTime 2021/11/24 - 18:38
 * @description 成功结果集
 */
@Builder
@Getter
@ToString
public class SuccessInfo extends ResultInfo {

    protected static final Integer DEFAULT_CODE = 20000;
    protected static final String DEFAULT_MESSAGE = "操作成功";

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected Object data;


    protected SuccessInfo(Object data) {
        super(true, DEFAULT_CODE, DEFAULT_MESSAGE);
        this.data = data;
    }
}
