package cn.baiqi.common.entity;

import lombok.Builder;
import lombok.Data;

/**
 * @author J3-白起
 * @package cn.baiqi.common.entity
 * @createTime 2021/11/25 - 10:08
 * @description
 */
@Data
public class SysUser {

    private String name;

}
