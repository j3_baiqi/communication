package cn.baiqi.common.config;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author J3-白起
 * @package cn.baiqi.common.config
 * @createTime 2021/11/28 - 0:08
 * @description 统一mapper文件路径扫描
 */
@Slf4j
@Configuration
@MapperScan("cn.baiqi.*.mapper")
public class MapperScanConfig {
}
