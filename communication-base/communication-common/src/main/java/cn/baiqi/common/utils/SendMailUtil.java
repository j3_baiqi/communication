package cn.baiqi.common.utils;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Objects;

/**
 * @author J3-白起
 * @package cn.baiqi.common.utils
 * @createTime 2021/12/21 - 19:23
 * @description
 */
@Slf4j
public class SendMailUtil {

    private static String url = "http://101.33.214.46:8500/communication-support-mail/v1/mail/sendBatch";
    //private static String url = "http://localhost:8500/communication-support-mail/v1/mail/sendBatch";

    public static String target01 = "1491989462@qq.com";

    public static void send(String title, Exception e, List<String> accountList){

        String error = parseStackTrace(e);
        if (Objects.isNull(error) || error.contains("无法构建Exception")){
            return;
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("addressList", accountList);
        jsonObject.put("title", title);
        jsonObject.put("content", "<xmp>" + error + "</xmp>");
        jsonObject.put("isHtml", true);
        HttpRequest.post(url)
                .body(jsonObject.toString())
                .execute();
    }

    public static String parseStackTrace(Exception ex) {
        StringWriter sw = null;
        PrintWriter pw = null;
        try {
            sw = new StringWriter();
            pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            return sw.toString();
        } catch (Exception e) {
            // do Nothing
        }
        return null;
    }

}
