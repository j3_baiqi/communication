package cn.baiqi.common.config;

import cn.baiqi.common.interceptor.GetIpInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author J3-白起
 * @package cn.baiqi.common.config
 * @createTime 2021/12/28 - 8:44
 * @description
 */
@Configuration
public class MyWebMvcConfig implements WebMvcConfigurer {

    @Bean
    GetIpInterceptor getIpInterceptor() {
        return new GetIpInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getIpInterceptor()).addPathPatterns("/**");
    }
}
