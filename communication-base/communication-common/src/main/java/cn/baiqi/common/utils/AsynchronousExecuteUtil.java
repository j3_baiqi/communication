package cn.baiqi.common.utils;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * @author J3-白起
 * @package cn.baiqi.common.utils
 * @createTime 2021/12/6 - 17:47
 * @description 异步执行工具
 */
@Slf4j
public class AsynchronousExecuteUtil {

    private static Integer corePoolSize;
    private static Integer maximumPoolSize;
    private static Integer keepAliveTime;
    private static TimeUnit unit;
    private static BlockingQueue workQueue;
    private static ThreadFactory threadFactory;
    private static RejectedExecutionHandler rejectedExecutionHandler;

    private static final ThreadPoolExecutor myThreadPool;

    static {
        corePoolSize = Runtime.getRuntime().availableProcessors();
        maximumPoolSize = corePoolSize * 2;
        keepAliveTime = 2;
        unit = TimeUnit.MINUTES;
        workQueue = new LinkedBlockingQueue(corePoolSize * 10);
        threadFactory = new ThreadFactoryBuilder().setNameFormat("my-async-execute-util-").build();
        rejectedExecutionHandler = new ThreadPoolExecutor.CallerRunsPolicy();
        myThreadPool = new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                keepAliveTime,
                unit,
                workQueue,
                threadFactory,
                rejectedExecutionHandler
        );
    }

    public static void execute(Runnable command) {
        try {
            myThreadPool.execute(command);
        } catch (Exception e) {
            //错误处理
            log.error("============AsynchronousExecuteUtil_execute_error，{}", e);
        }

    }


}
