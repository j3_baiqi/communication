package cn.baiqi.common.support;

import cn.baiqi.common.utils.JasyptUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.Properties;

/**
 * @author J3-白起
 * @package cn.baiqi.common.utils
 * @createTime 2021/12/14 - 14:53
 * @description 资源加载
 */
@Slf4j
@Component
public class CommonResources {

    private final Properties pro = new Properties();
    private final String ENC_PREFIX = "ENC(";
    private final String ENC_SUFFIX = ")";

    public CommonResources() {
        log.info("===========start=========load_CommonResources");
        InputStream in = null;
        BufferedReader bf = null;
        try {
            ClassPathResource resource = new ClassPathResource("commun.properties");
            in = resource.getInputStream();
            //解决中文乱码
            bf = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            pro.load(bf);
            log.info("===========end=========load_CommonResources,{}", pro);
        } catch (IOException e) {
            log.error("=========init_Properties_erroe, {}", e);
        } finally {
            try {
                in.close();
                bf.close();
            } catch (IOException e) {
                log.error("=========init_Properties_erroe, {}", e);
            }
        }
    }

    public String getProperty(String key) {
        String property = pro.getProperty(key);
        if (Objects.nonNull(property) && property.contains(ENC_PREFIX)) {
            property = property.replace(ENC_PREFIX, "");
            property = property.substring(0, property.lastIndexOf(ENC_SUFFIX));
            return JasyptUtils.decrypt(property);
        }
        return pro.getProperty(key);
    }


}
