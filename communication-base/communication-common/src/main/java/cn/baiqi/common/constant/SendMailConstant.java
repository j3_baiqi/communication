package cn.baiqi.common.constant;

/**
 * @author J3-白起
 * @package cn.baiqi.common.constant
 * @createTime 2021/11/26 - 15:20
 * @description 发送邮件常量
 */
public class SendMailConstant {
    /**
     * 邮件重发次数
     */
    public static final Integer RETRY_NUMBER = 3;

}
