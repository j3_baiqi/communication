package cn.baiqi.common.interceptor;

import cn.baiqi.common.annotation.GetIP;
import cn.baiqi.common.utils.IpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * @author J3-白起
 * @package cn.baiqi.common.interceptor
 * @createTime 2021/12/28 - 8:41
 * @description
 */
@Slf4j
public class GetIpInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            GetIP annotation = handlerMethod.getMethod().getAnnotation(GetIP.class);
            if (!Objects.isNull(annotation)) {
                IpUtils.setIp(request);
            }
        }

        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        IpUtils.delete(request);
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
