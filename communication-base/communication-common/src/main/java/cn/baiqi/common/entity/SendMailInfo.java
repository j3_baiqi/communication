package cn.baiqi.common.entity;

import lombok.Data;

/**
 * @author J3-白起
 * @package cn.baiqi.common.entity
 * @createTime 2021/11/25 - 16:05
 * @description
 */
@Data
public class SendMailInfo {
    /**
     * 对方地址
     */
    private String address;

    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;

    /**
     * 内容是否为html
     */
    private Boolean isHtml;

}
