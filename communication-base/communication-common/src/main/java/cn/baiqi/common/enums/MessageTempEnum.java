package cn.baiqi.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

/**
 * @author J3-白起
 * @package cn.baiqi.common.enums
 * @createTime 2021/11/28 - 17:44
 * @description 模板类型
 */
@Getter
public enum MessageTempEnum {

    MESSAGE_TEMP_ENUM_MAIL_1("邮件类型_默认", 1),

    MESSAGE_TEMP_ENUM_MAIL_2("邮件类型_验证", 2),

    MESSAGE_TEMP_ENUM_MAIL_3("邮件类型_消息", 3),

    MESSAGE_TEMP_ENUM_QQBOT_4("qqbot类型_默认", 4),

    MESSAGE_TEMP_ENUM_QQBOT_5("qqbot类型_验证", 5),

    MESSAGE_TEMP_ENUM_QQBOT_6("qqbot类型_消息", 6),

    ;

    @EnumValue
    public Integer value;
    public String description;

    MessageTempEnum(String description, Integer value) {
        this.value = value;
        this.description = description;
    }
}
