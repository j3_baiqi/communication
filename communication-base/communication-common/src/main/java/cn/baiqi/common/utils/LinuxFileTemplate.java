package cn.baiqi.common.utils;

import cn.baiqi.common.exception.SysException;
import cn.baiqi.common.support.CommonResources;
import cn.hutool.core.io.FileTypeUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileWriter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author J3-白起
 * @package cn.baiqi.common.utils
 * @createTime 2021/12/14 - 12:24
 * @description
 */
@Slf4j
@Component
@AllArgsConstructor
public class LinuxFileTemplate {

    private final CommonResources commonResources;
    private final String pact = "linux.file.pact";
    private final String ip = "linux.file.ip";
    private final String path = "linux.file.path";
    private final String defaultName = "linux.file.path.defaultName";
    private final String slash = "/";
    private final SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

    public String upload(MultipartFile file) {
        String type = "";
        try {
            type = FileTypeUtil.getType(file.getInputStream());
        } catch (IOException e) {
            log.error("=========upload_error，{}", e);
            throw new SysException("文件上传失败！");
        }

        if (Objects.isNull(type)) {
            type = defaultName;
        }

        //生成文件目录
        String format = this.format.format(new Date());
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String filePath = commonResources.getProperty(path) + type + slash + format + slash + uuid + file.getOriginalFilename();

        //创建文件，通过路径
        FileWriter writer = new FileWriter(FileUtil.touch(filePath));
        try {
            writer.writeFromStream(file.getInputStream());
        } catch (IOException e) {
            log.error("=========upload_error，{}", e);
            throw new SysException("文件上传失败！");
        }
        return commonResources.getProperty(pact) + commonResources.getProperty(ip) + filePath;
    }

    public List<String> upload(MultipartFile[] files) {
        List<String> usrList = new ArrayList<>(files.length);
        for (MultipartFile file : files) {
            usrList.add(upload(file));
        }
        return usrList;
    }

    public void delete(String url) {
        // 处理 url
        log.info("============入参：{}", url);
        String substring = url.substring(("http://" + commonResources.getProperty(ip)).length());
        if (url.contains("G:\\")) {
            substring = url;
        }
        log.info("============substring：{}", substring);
        FileUtil.del(substring);
    }

    public void delete(List<String> urlList) {
        for (String url : urlList) {
            delete(url);
        }
    }

}
