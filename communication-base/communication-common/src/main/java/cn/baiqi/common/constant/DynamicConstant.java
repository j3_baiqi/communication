package cn.baiqi.common.constant;

import java.time.Duration;

/**
 * @author J3-白起
 * @package cn.baiqi.common.constant
 * @createTime 2021/12/31 - 23:37
 * @description
 */
public class DynamicConstant {

    /**
     * 分享业务，redis key 前缀
     */
    public static final String PUBLISH_SHARE_KEY = "publish:share:";
    public static final Duration LIMIT_PUBLISH_TIME = Duration.ofMinutes(5);


}
