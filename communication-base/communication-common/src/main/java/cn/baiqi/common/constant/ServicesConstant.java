package cn.baiqi.common.constant;

/**
 * @author J3-白起
 * @package cn.baiqi.common.enums
 * @createTime 2021/11/26 - 9:35
 * @description 服务名常量
 */
public class ServicesConstant {
    /**
     * social服务模块
     */
    public static final String SOCIAL = "communication-social";

    /**
     * mail服务模块
     */
    public static final String MAIL = "communication-support-mail";

    /**
     * qqboot服务模块
     */
    public static final String QQ_BOT = "communication-support-qqbot";
}
