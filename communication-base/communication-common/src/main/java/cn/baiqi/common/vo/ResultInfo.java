package cn.baiqi.common.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author J3-白起
 * @package cn.baiqi.common.vo
 * @createTime 2021/11/24 - 18:36
 * @description 抽象统一结果
 */
@Getter
public abstract class ResultInfo implements Serializable {

    protected Boolean result;
    protected Integer code;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected String message;

    protected ResultInfo(Boolean result, Integer code, String message) {
        this.result = result;
        this.code = code;
        this.message = message;
    }

}
