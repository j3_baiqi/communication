package cn.baiqi.socialcore.dto;

import cn.baiqi.common.entity.SysUser;
import lombok.Builder;
import lombok.Data;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.dto
 * @createTime 2021/11/25 - 10:08
 * @description
 */
@Data
public class SysUserDTO extends SysUser {

    private Integer id;

}
