package cn.baiqi.socialcore.api.v1.controller;

import cn.baiqi.common.annotation.ResponseResult;
import cn.baiqi.socialcore.api.form.ArticleForm;
import cn.baiqi.socialcore.api.query.ArticleQuery;
import cn.baiqi.socialcore.api.vo.ArticleVO;
import cn.baiqi.socialcore.bo.ArticleBO;
import cn.baiqi.socialcore.converter.SysConverter;
import cn.baiqi.socialcore.po.Article;
import cn.baiqi.socialcore.service.ArticleService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.v1.controller
 * @createTime 2021/12/29 - 23:44
 * @description
 */
@Slf4j
@AllArgsConstructor
@ResponseResult
@RequestMapping("/v1/article")
public class ArticleController {


    private final ArticleService service;
    private final SysConverter converter;


    @GetMapping("/")
    public IPage<ArticleVO> getPage(Page<Article> page, @Validated ArticleQuery query) {
        IPage<ArticleBO> iPage = service.getPage(page, query);
        return iPage.convert(converter::converter);
    }

    @GetMapping("/one/{id}")
    public ArticleVO getOne(@PathVariable(name = "id") Long id) {
        return converter.converterVO(service.getById(id));
    }

    @PostMapping("/")
    public void save(@RequestBody ArticleForm form) {
        service.saveOrUpdate(converter.converter(form));
    }

    @GetMapping("/delete")
    public void delete(@RequestParam(name = "id") Long id) {
        service.removeById(id);
    }

}
