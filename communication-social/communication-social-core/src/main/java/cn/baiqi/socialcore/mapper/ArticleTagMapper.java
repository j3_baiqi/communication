package cn.baiqi.socialcore.mapper;

import cn.baiqi.socialcore.po.ArticleTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * @package cn.baiqi.socialcore.mapper
 * @author J3-白起
 * @createTime 2021/12/29 - 23:53
 * @description ${Description}
 */
public interface ArticleTagMapper extends BaseMapper<ArticleTag> {
    int updateBatch(List<ArticleTag> list);

    int updateBatchSelective(List<ArticleTag> list);

    int batchInsert(@Param("list") List<ArticleTag> list);

    int insertOrUpdate(ArticleTag record);

    int insertOrUpdateSelective(ArticleTag record);
}