package cn.baiqi.socialcore.api.vo;

import cn.baiqi.socialcore.po.DynamicComments;
import lombok.Data;

@Data
public class DynamicCommentsVO extends DynamicComments {

}