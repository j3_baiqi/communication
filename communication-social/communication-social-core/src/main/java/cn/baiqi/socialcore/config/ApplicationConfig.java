package cn.baiqi.socialcore.config;

import cn.baiqi.mailapi.EnableMailFeignClients;
import cn.baiqi.qqbotapi.EnableQqBotFeignClients;
import org.springframework.context.annotation.Configuration;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.config
 * @createTime 2021/11/26 - 12:23
 * @description
 */
@Configuration
@EnableMailFeignClients
@EnableQqBotFeignClients
public class ApplicationConfig {
}
