package cn.baiqi.socialcore.api.form;

import cn.baiqi.socialcore.po.Article;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.vo
 * @createTime 2021/12/30 - 8:17
 * @description
 */
@Data
public class ArticleForm extends Article {

    /**
     * 标题
     */
    @NotNull(message = "标题不空")
    @Length(max = 24, message = "标题请限制再24字符内")
    private String title;

    /**
     * 简介
     */
    @NotNull(message = "简介不空")
    @Length(max = 85, message = "简介请限制再85字符内")
    private String introduction;

    /**
     * 文章标签id
     */
    @NotNull(message = "文章标签不空")
    private Long articleTagId;

}
