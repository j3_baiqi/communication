package cn.baiqi.socialcore.api.query;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.vo
 * @createTime 2021/12/30 - 8:17
 * @description
 */
@Data
public class ArticleQuery {
    @NotNull(message = "文章类型不为空")
    private Long articleTagId;

}
