package cn.baiqi.socialcore.api.vo;

import cn.baiqi.socialcore.po.Dynamic;
import lombok.Data;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.vo
 * @createTime 2021/12/27 - 9:27
 * @description
 */
@Data
public class DynamicVO extends Dynamic {
}
