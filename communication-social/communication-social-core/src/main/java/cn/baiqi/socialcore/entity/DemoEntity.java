package cn.baiqi.socialcore.entity;

import lombok.Builder;
import lombok.Data;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.entity
 * @createTime 2021/11/24 - 23:28
 * @description
 */
@Builder
@Data
public class DemoEntity {

    private String name;
    private Integer age;

}
