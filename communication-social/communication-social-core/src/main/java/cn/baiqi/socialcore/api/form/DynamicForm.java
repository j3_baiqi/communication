package cn.baiqi.socialcore.api.form;

import cn.baiqi.socialcore.po.Dynamic;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.form
 * @createTime 2021/12/27 - 9:28
 * @description
 */
@Data
public class DynamicForm extends Dynamic {

    @NotNull(message = "标题不空")
    @Length(max = 16, message = "请限制再16字符内")
    private String titleOrNickname;

    @NotNull(message = "内容不空")
    private String content;


}
