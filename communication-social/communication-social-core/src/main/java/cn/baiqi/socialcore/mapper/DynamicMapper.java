package cn.baiqi.socialcore.mapper;

import cn.baiqi.socialcore.api.query.DynamicQuery;
import cn.baiqi.socialcore.po.Dynamic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity cn.baiqi.socialcore.po.Dynamic
 */
public interface DynamicMapper extends BaseMapper<Dynamic> {

    IPage<Dynamic> getPage(@Param("page") Page<Dynamic> page, @Param("query") DynamicQuery query);
}




