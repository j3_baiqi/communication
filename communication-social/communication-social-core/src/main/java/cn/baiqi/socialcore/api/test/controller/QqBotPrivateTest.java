package cn.baiqi.socialcore.api.test.controller;

import cn.baiqi.common.annotation.ResponseResult;
import cn.baiqi.qqbotapi.dto.PrivateMessageDTO;
import cn.baiqi.qqbotapi.v1.QqBotPrivate;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.v1.controller
 * @createTime 2021/11/28 - 0:05
 * @description
 */
@Slf4j
@AllArgsConstructor
@ResponseResult
@RequestMapping("/test/qqbot/pr")
public class QqBotPrivateTest {

    private final QqBotPrivate qqBotPrivate;

    @GetMapping("/send")
    public void sendMessage(@RequestParam(name = "targetAccount") String targetAccount, @RequestParam(name = "content") String content) {
        var privateMessageDTO = new PrivateMessageDTO()
                .setTargetAccount(targetAccount)
                .setContent(content);
        qqBotPrivate.sendMessage(privateMessageDTO);
    }


}
