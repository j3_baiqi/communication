package cn.baiqi.socialcore.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @TableName comm_dynamic_comments
 */
@TableName(value ="comm_dynamic_comments")
@Data
public class DynamicComments implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 动态id
     */
    private Long dynamicId;

    /**
     * 父级评论id
     */
    private Long parentId;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 内容
     */
    private String comments;

    /**
     * 创建事件
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改事件
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}