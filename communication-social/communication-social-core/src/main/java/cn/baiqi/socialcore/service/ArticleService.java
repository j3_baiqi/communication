package cn.baiqi.socialcore.service;

import cn.baiqi.socialcore.api.query.ArticleQuery;
import cn.baiqi.socialcore.bo.ArticleBO;
import cn.baiqi.socialcore.po.Article;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface ArticleService extends IService<Article> {

    IPage<ArticleBO> getPage(Page<Article> page, ArticleQuery query);
}
