package cn.baiqi.socialcore.po;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 
 * @TableName comm_dynamic
 */
@TableName(value ="comm_dynamic", autoResultMap = true)
@Data
public class Dynamic implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 标题或昵称
     */
    private String titleOrNickname;

    /**
     * 内容
     */
    private String content;

    /**
     * 图片
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> imageList;

    /**
     * 图片
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> tagList;

    /**
     * 评论数
     */
    private Integer comments;

    /**
     * 点赞数
     */
    private Integer giveLike;

    /**
     * 创建事件
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改事件
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}