package cn.baiqi.socialcore.api.v1.controller;

import cn.baiqi.common.annotation.ResponseResult;
import cn.baiqi.socialcore.api.form.ArticleTagForm;
import cn.baiqi.socialcore.api.query.ArticleTagQuery;
import cn.baiqi.socialcore.api.vo.ArticleTagVO;
import cn.baiqi.socialcore.bo.ArticleTagBO;
import cn.baiqi.socialcore.converter.SysConverter;
import cn.baiqi.socialcore.po.ArticleTag;
import cn.baiqi.socialcore.service.ArticleTagService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.v1.controller
 * @createTime 2021/12/29 - 23:44
 * @description
 */
@Slf4j
@AllArgsConstructor
@ResponseResult
@RequestMapping("/v1/articleTag")
public class ArticleTagController {


    private final ArticleTagService service;
    private final SysConverter converter;


    @GetMapping("/")
    public IPage<ArticleTagVO> getPage(Page<ArticleTag> page, ArticleTagQuery query) {
        IPage<ArticleTagBO> iPage = service.getPage(page, query);
        return iPage.convert(converter::converter);
    }

    @PostMapping("/")
    public void save(@RequestBody ArticleTagForm form){
        service.insertOrUpdate(converter.converter(form));
    }

    @GetMapping("/delete")
    public void delete(@RequestParam(name = "id") Long id){
        service.removeById(id);
    }

}
