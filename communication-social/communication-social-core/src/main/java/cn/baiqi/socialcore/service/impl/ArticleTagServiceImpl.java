package cn.baiqi.socialcore.service.impl;

import cn.baiqi.socialcore.api.query.ArticleTagQuery;
import cn.baiqi.socialcore.bo.ArticleTagBO;
import cn.baiqi.socialcore.converter.SysConverter;
import cn.baiqi.socialcore.mapper.ArticleTagMapper;
import cn.baiqi.socialcore.po.ArticleTag;
import cn.baiqi.socialcore.service.ArticleTagService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * @package cn.baiqi.socialcore.service.impl
 * @author J3-白起
 * @createTime 2021/12/29 - 23:53
 * @description ${Description}
 */
@Slf4j
@Data
@Service
@AllArgsConstructor
public class ArticleTagServiceImpl extends ServiceImpl<ArticleTagMapper, ArticleTag> implements ArticleTagService{

    private final SysConverter converter;

    @Override
    public int updateBatch(List<ArticleTag> list) {
        return baseMapper.updateBatch(list);
    }
    @Override
    public int updateBatchSelective(List<ArticleTag> list) {
        return baseMapper.updateBatchSelective(list);
    }
    @Override
    public int batchInsert(List<ArticleTag> list) {
        return baseMapper.batchInsert(list);
    }
    @Override
    public int insertOrUpdate(ArticleTag record) {
        return baseMapper.insertOrUpdate(record);
    }
    @Override
    public int insertOrUpdateSelective(ArticleTag record) {
        return baseMapper.insertOrUpdateSelective(record);
    }

    @Override
    public IPage<ArticleTagBO> getPage(Page<ArticleTag> page, ArticleTagQuery query) {
        IPage<ArticleTag> articleTagPage = lambdaQuery()
                .orderBy(true, false, List.of(ArticleTag::getOrder, ArticleTag::getUpdateTime))
                .page(page);
        return articleTagPage.convert(converter::converter);
    }
}
