package cn.baiqi.socialcore.api.test.controller;

import cn.baiqi.common.annotation.ResponseResult;
import cn.baiqi.common.entity.SysUser;
import cn.baiqi.common.exception.SysException;
import cn.baiqi.socialcore.converter.SysConverter;
import cn.baiqi.socialcore.dto.SysUserDTO;
import cn.baiqi.socialcore.entity.DemoEntity;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.v1.controller
 * @createTime 2021/11/24 - 16:30
 * @description
 */
@Slf4j
@AllArgsConstructor
@ResponseResult
@RequestMapping("/test")
public class TestController {

    private final SysConverter converter;
    private final RedisTemplate<String, String> redisTemplate;

    @GetMapping("")
    public String test() {
        log.info("test...");
        return "success";
    }

    @GetMapping("/errer")
    public String errorTest() {
        log.info("test...");
        throw new SysException("哎呀！%s,出错了", "errorTest");
        // return "success";
    }

    @GetMapping("/list")
    public List<DemoEntity> list() {
        DemoEntity user = DemoEntity.builder().name("j3_baiqi").age(18).build();
        List<DemoEntity> resultList = List.of(user, user, user);
        //return resultList;
        return null;
    }

    @GetMapping("/controller")
    public List<SysUserDTO> controller() {

        SysUser sysUser = new SysUser();
        sysUser.setName("J3-baiqi");
        List<SysUser> sysUserList = List.of(sysUser);

        List<SysUserDTO> sysUserDTOList = converter.converter(sysUserList);

        return sysUserDTOList;
    }

    @GetMapping("/redis")
    public String redisTest() {

        redisTemplate.opsForValue().set("k1","v1");

        String s = redisTemplate.opsForValue().get("k1");

        return null;
    }
    @GetMapping("/redis02")
    public void redisTest02() {

        redisTemplate.opsForValue().set("k1","v1");

        String s = redisTemplate.opsForValue().get("k1");

    }

}
