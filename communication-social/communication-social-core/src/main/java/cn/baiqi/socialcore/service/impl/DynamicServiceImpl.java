package cn.baiqi.socialcore.service.impl;

import cn.baiqi.common.constant.DynamicConstant;
import cn.baiqi.common.exception.SysException;
import cn.baiqi.common.utils.IpUtils;
import cn.baiqi.common.utils.LinuxFileTemplate;
import cn.baiqi.common.utils.MyOssTemplate;
import cn.baiqi.socialcore.api.form.DynamicForm;
import cn.baiqi.socialcore.api.query.DynamicQuery;
import cn.baiqi.socialcore.bo.DynamicBO;
import cn.baiqi.socialcore.converter.SysConverter;
import cn.baiqi.socialcore.mapper.DynamicMapper;
import cn.baiqi.socialcore.po.Dynamic;
import cn.baiqi.socialcore.service.DynamicService;
import com.alibaba.nacos.common.utils.Objects;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 *
 */
@Slf4j
@Data
@Service
@AllArgsConstructor
public class DynamicServiceImpl extends ServiceImpl<DynamicMapper, Dynamic>
        implements DynamicService {

    private final DynamicMapper mapper;
    private final SysConverter converter;
    private final RedisTemplate<String, Object> redisTemplate;
    private final LinuxFileTemplate linuxFileTemplate;
    private final MyOssTemplate myOssTemplate;

    @Override
    public IPage<DynamicBO> getPage(Page<Dynamic> page, DynamicQuery query) {
        final var dynamicIPage = mapper.getPage(page, query);
        return dynamicIPage.convert(converter::converter);
    }

    @Override
    public void checkAndSave(DynamicForm form) {

        String ip = IpUtils.getIp();
        log.info("user_IP,{}", ip);

        if (Objects.isNull(ip)) {
            deleteImage(form);
            throw new SysException("J3说，拿不到你的IP，不可发布！");
        }

        String key = DynamicConstant.PUBLISH_SHARE_KEY + ip;

        if (Objects.nonNull(redisTemplate.opsForValue().get(key))) {
            deleteImage(form);
            throw new SysException("亲太频繁了，五分钟后再来呗！");
        }

        // 五分钟内没有提交过内容的用户，可以发布
        redisTemplate.opsForValue().set(key, key, DynamicConstant.LIMIT_PUBLISH_TIME);

        Dynamic dynamic = converter.converter(form);

        save(dynamic);
    }

    private void deleteImage(DynamicForm form) {
        linuxFileTemplate.delete(form.getImageList());
        myOssTemplate.delete(form.getImageList());
    }
}




