package cn.baiqi.socialcore.api.test.controller;

import cn.baiqi.common.annotation.ResponseResult;
import cn.baiqi.mailapi.dto.BatchSendMailInfoDTO;
import cn.baiqi.mailapi.v1.SendMailApi;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.v1.controller
 * @createTime 2021/11/25 - 17:21
 * @description
 */
@Slf4j
@AllArgsConstructor
@ResponseResult
@RequestMapping("/test/mail")
public class MailTestController {

    private final SendMailApi sendMailApi;


    @GetMapping("")
    public String mail(@RequestParam(name = "content", required = true) String content) {

        List<String> targetMailList = List.of(
                "1491989462@qq.com"
        );
        List<String> targetMailList1 = List.of(
                "1491989462@qq.com",
                "1141617850@qq.com",
                "2994435150@qq.com",
                "1781913075@qq.com",
                "1460838887@qq.com",
                "1205315867@qq.com",
                "axin281012@163.com",
                "1149771839@qq.com"
        );
        List<String> targetMailList2 = List.of(
                "Lineer2394876572@outlook.com"
        );
        BatchSendMailInfoDTO sendMailInfoDTO = new BatchSendMailInfoDTO();
        sendMailInfoDTO.setAddressList(targetMailList);
        sendMailInfoDTO.setTitle("communication邮件服务");
        sendMailInfoDTO.setContent(content);
        sendMailInfoDTO.setIsHtml(false);
        sendMailApi.sendMail(sendMailInfoDTO);
        return "success";
    }
}
