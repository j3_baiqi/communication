package cn.baiqi.socialcore.api.form;

import cn.baiqi.socialcore.po.DynamicComments;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DynamicCommentsForm extends DynamicComments {


    /**
     * 动态id
     */
    @NotNull(message = "动态id不为空")
    private Long dynamicId;

    /**
     * 内容
     */
    @NotNull(message = "评论不为空")
    private String comments;

}