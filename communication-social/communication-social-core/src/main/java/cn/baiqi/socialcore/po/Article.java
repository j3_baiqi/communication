package cn.baiqi.socialcore.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @TableName comm_article
 */
@TableName(value ="comm_article")
@Data
public class Article implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 文章标签id
     */
    private Long articleTagId;

    /**
     * 标题
     */
    private String title;

    /**
     * 简介
     */
    private String introduction;

    /**
     * 小图标url
     */
    private String imageUrl;

    /**
     * csdn文章地址
     */
    private String csdnUrl;

    /**
     * 掘金文章地址
     */
    private String juejinUrl;

    /**
     * 知乎文章地址
     */
    private String zhihuUrl;

    /**
     * 创建事件
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改事件
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}