package cn.baiqi.socialcore.converter;

import cn.baiqi.common.entity.SysUser;
import cn.baiqi.socialcore.api.form.ArticleForm;
import cn.baiqi.socialcore.api.form.ArticleTagForm;
import cn.baiqi.socialcore.api.form.DynamicForm;
import cn.baiqi.socialcore.api.vo.ArticleTagVO;
import cn.baiqi.socialcore.api.vo.ArticleVO;
import cn.baiqi.socialcore.api.vo.DynamicCommentsVO;
import cn.baiqi.socialcore.api.vo.DynamicVO;
import cn.baiqi.socialcore.bo.ArticleBO;
import cn.baiqi.socialcore.bo.ArticleTagBO;
import cn.baiqi.socialcore.bo.DynamicBO;
import cn.baiqi.socialcore.bo.DynamicCommentsBO;
import cn.baiqi.socialcore.dto.SysUserDTO;
import cn.baiqi.socialcore.po.Article;
import cn.baiqi.socialcore.po.ArticleTag;
import cn.baiqi.socialcore.po.Dynamic;
import cn.baiqi.socialcore.po.DynamicComments;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.converter
 * @createTime 2021/11/25 - 10:11
 * @description
 */
@Mapper(componentModel = "spring", typeConversionPolicy = ReportingPolicy.ERROR)//交给spring管理
public interface SysConverter {

    List<SysUserDTO> converter(List<SysUser> sysUserList);

    SysUserDTO converter(SysUser sysUserList);

    DynamicVO converter(DynamicBO dynamicBO);

    ArticleTagVO converter(ArticleTagBO articleTagBO);

    ArticleTagBO converter(ArticleTag articleTag);

    ArticleTag converter(ArticleTagForm form);

    Article converter(ArticleForm form);

    ArticleVO converter(ArticleBO articleBO);

    ArticleBO converter(Article article);

    ArticleVO converterVO(Article article);

    Dynamic converter(DynamicForm form);

    List<DynamicCommentsVO> converterCommentsVO(List<DynamicCommentsBO> commentsBOList);

    DynamicCommentsVO converter(DynamicCommentsBO commentsBO);

    List<DynamicCommentsBO> converterCommentsBO(List<DynamicComments> dynamicCommentsList);

    DynamicCommentsBO converter(DynamicComments dynamicComments);

    DynamicBO converter(Dynamic dynamic);
}
