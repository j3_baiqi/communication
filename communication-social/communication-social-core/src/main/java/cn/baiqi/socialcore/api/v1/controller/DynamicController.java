package cn.baiqi.socialcore.api.v1.controller;

import cn.baiqi.common.annotation.GetIP;
import cn.baiqi.common.annotation.ResponseResult;
import cn.baiqi.socialcore.api.form.DynamicForm;
import cn.baiqi.socialcore.api.query.DynamicQuery;
import cn.baiqi.socialcore.api.vo.DynamicVO;
import cn.baiqi.socialcore.converter.SysConverter;
import cn.baiqi.socialcore.po.Dynamic;
import cn.baiqi.socialcore.service.DynamicService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.v1.controller
 * @createTime 2021/12/27 - 9:23
 * @description 动态
 */
@Slf4j
@AllArgsConstructor
@ResponseResult
@RequestMapping("/v1/dynamic")
public class DynamicController {

    private final DynamicService service;
    private final SysConverter converter;

    @GetMapping("")
    public IPage<DynamicVO> getPage(Page<Dynamic> page, DynamicQuery query) {
        var iPage = service.getPage(page, query);
        return iPage.convert(converter::converter);
    }

    @GetIP
    @PostMapping("")
    public void save(@RequestBody @Validated DynamicForm form) {
        service.checkAndSave(form);
    }





}
