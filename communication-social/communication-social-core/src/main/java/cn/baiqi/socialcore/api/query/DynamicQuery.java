package cn.baiqi.socialcore.api.query;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.query
 * @createTime 2021/12/27 - 9:25
 * @description 动态查询对象
 */
@Accessors(chain = true)
@Data
public class DynamicQuery {
}
