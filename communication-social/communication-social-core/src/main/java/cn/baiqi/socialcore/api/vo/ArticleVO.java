package cn.baiqi.socialcore.api.vo;

import cn.baiqi.socialcore.po.Article;
import lombok.Data;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.vo
 * @createTime 2021/12/30 - 8:17
 * @description
 */
@Data
public class ArticleVO extends Article {
}
