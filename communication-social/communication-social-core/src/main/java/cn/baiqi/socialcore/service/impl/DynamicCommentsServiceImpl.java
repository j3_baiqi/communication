package cn.baiqi.socialcore.service.impl;

import cn.baiqi.socialcore.api.form.DynamicCommentsForm;
import cn.baiqi.socialcore.bo.DynamicCommentsBO;
import cn.baiqi.socialcore.converter.SysConverter;
import cn.baiqi.socialcore.mapper.DynamicCommentsMapper;
import cn.baiqi.socialcore.po.DynamicComments;
import cn.baiqi.socialcore.service.DynamicCommentsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Slf4j
@Data
@Service
@AllArgsConstructor
public class DynamicCommentsServiceImpl extends ServiceImpl<DynamicCommentsMapper, DynamicComments>
        implements DynamicCommentsService {

    private final SysConverter converter;

    @Override
    public List<DynamicCommentsBO> list(Long dynamicId) {

        final var dynamicCommentsList = lambdaQuery().eq(DynamicComments::getDynamicId, dynamicId)
                .orderByDesc(List.of(DynamicComments::getCreateTime)).list();

        return converter.converterCommentsBO(dynamicCommentsList);
    }

    @Override
    public void comment(DynamicCommentsForm form) {

        save(converter.converter(form));

    }
}




