package cn.baiqi.socialcore.api.test.controller;

import cn.baiqi.common.annotation.ResponseResult;
import cn.baiqi.common.support.CommonResources;
import cn.baiqi.common.utils.LinuxFileTemplate;
import cn.baiqi.common.utils.MyOssTemplate;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.v1.controller
 * @createTime 2021/12/14 - 11:24
 * @description
 */
@Slf4j
@AllArgsConstructor
@ResponseResult
@RequestMapping("/test/oss")
public class OssTemplateTest {

    private final MyOssTemplate myOssTemplate;
    private final LinuxFileTemplate linuxFileTemplate;
    private final CommonResources commonResources;

    @PostMapping("/upload")
    public String upload(MultipartFile file) {
        return myOssTemplate.upload(file);
        //return linuxFileTemplate.upload(file);
    }

    @PostMapping("/batch")
    public List<String> upload(MultipartFile[] files) {
        return myOssTemplate.upload(files);
    }

    @GetMapping("/delete")
    public void delete(@RequestParam(name = "url") String url) {
        myOssTemplate.delete(url);
        //linuxFileTemplate.delete(url);
    }

    @PostMapping("/deletes")
    public void deletes(@RequestBody List<String> urlList) {
        myOssTemplate.delete(urlList);
    }


    @GetMapping("/key")
    public String getKey(@RequestParam(name = "key") String key) {
        return commonResources.getProperty(key);
    }

}
