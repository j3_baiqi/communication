package cn.baiqi.socialcore.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @package cn.baiqi.socialcore.po
 * @author J3-白起
 * @createTime 2021/12/29 - 23:53
 * @description ${Description}
 */
@Data
@TableName(value = "comm_article_tag")
public class ArticleTag {
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @TableField(value = "`name`")
    private String name;

    @TableField(value = "`order`")
    private Integer order;

    /**
     * 创建事件
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改事件
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    public static final String COL_ID = "id";

    public static final String COL_NAME = "name";

    public static final String COL_ORDER = "order";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";
}