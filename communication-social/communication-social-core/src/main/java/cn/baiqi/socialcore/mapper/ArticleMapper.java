package cn.baiqi.socialcore.mapper;

import cn.baiqi.socialcore.po.Article;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity cn.baiqi.socialcore.po.Article
 */
public interface ArticleMapper extends BaseMapper<Article> {

}




