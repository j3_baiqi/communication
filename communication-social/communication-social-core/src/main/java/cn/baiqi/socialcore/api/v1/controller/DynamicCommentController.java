package cn.baiqi.socialcore.api.v1.controller;

import cn.baiqi.common.annotation.ResponseResult;
import cn.baiqi.socialcore.api.form.DynamicCommentsForm;
import cn.baiqi.socialcore.api.vo.DynamicCommentsVO;
import cn.baiqi.socialcore.converter.SysConverter;
import cn.baiqi.socialcore.service.DynamicCommentsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.v1.controller
 * @createTime 2022/1/1 - 10:34
 * @description
 */
@Slf4j
@AllArgsConstructor
@ResponseResult
@RequestMapping("/v1/dynamicComment")
public class DynamicCommentController {


    private final DynamicCommentsService service;
    private final SysConverter converter;

    @GetMapping("/{dynamicId}")
    public List<DynamicCommentsVO> list(@PathVariable(name = "dynamicId") Long dynamicId) {
        return converter.converterCommentsVO(service.list(dynamicId));
    }

    @PostMapping("/comment")
    public void comment(@RequestBody @Validated DynamicCommentsForm form) {
        service.comment(form);
    }


}
