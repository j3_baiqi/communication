package cn.baiqi.socialcore.service;

import cn.baiqi.socialcore.api.form.DynamicForm;
import cn.baiqi.socialcore.api.query.DynamicQuery;
import cn.baiqi.socialcore.bo.DynamicBO;
import cn.baiqi.socialcore.po.Dynamic;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface DynamicService extends IService<Dynamic> {

    IPage<DynamicBO> getPage(Page<Dynamic> page, DynamicQuery query);

    void checkAndSave(DynamicForm form);
}
