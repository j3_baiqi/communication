package cn.baiqi.socialcore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocialCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocialCoreApplication.class, args);
    }

}
