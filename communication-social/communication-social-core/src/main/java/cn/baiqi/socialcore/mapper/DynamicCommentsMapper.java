package cn.baiqi.socialcore.mapper;

import cn.baiqi.socialcore.po.DynamicComments;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity cn.baiqi.socialcore.po.DynamicComments
 */
public interface DynamicCommentsMapper extends BaseMapper<DynamicComments> {

}




