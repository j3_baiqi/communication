package cn.baiqi.socialcore.service.impl;

import cn.baiqi.socialcore.api.query.ArticleQuery;
import cn.baiqi.socialcore.bo.ArticleBO;
import cn.baiqi.socialcore.converter.SysConverter;
import cn.baiqi.socialcore.mapper.ArticleMapper;
import cn.baiqi.socialcore.po.Article;
import cn.baiqi.socialcore.service.ArticleService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 *
 */
@Slf4j
@Data
@Service
@AllArgsConstructor
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article>
    implements ArticleService{

    private final SysConverter converter;

    @Override
    public IPage<ArticleBO> getPage(Page<Article> page, ArticleQuery query) {
        IPage<Article> articlePage = lambdaQuery()
                .eq(Objects.nonNull(query.getArticleTagId()), Article::getArticleTagId, query.getArticleTagId())
                .orderBy(true, false, List.of(Article::getUpdateTime))
                .page(page);
        return articlePage.convert(converter::converter);
    }
}




