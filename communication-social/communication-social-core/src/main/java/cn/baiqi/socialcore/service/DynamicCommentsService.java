package cn.baiqi.socialcore.service;

import cn.baiqi.socialcore.api.form.DynamicCommentsForm;
import cn.baiqi.socialcore.bo.DynamicCommentsBO;
import cn.baiqi.socialcore.po.DynamicComments;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *
 */
public interface DynamicCommentsService extends IService<DynamicComments> {

    List<DynamicCommentsBO> list(Long dynamicId);

    void comment(DynamicCommentsForm form);
}
