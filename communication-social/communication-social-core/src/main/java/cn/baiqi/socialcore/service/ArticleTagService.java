package cn.baiqi.socialcore.service;

import cn.baiqi.socialcore.api.query.ArticleTagQuery;
import cn.baiqi.socialcore.bo.ArticleTagBO;
import cn.baiqi.socialcore.po.ArticleTag;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * @package cn.baiqi.socialcore.service
 * @author J3-白起
 * @createTime 2021/12/29 - 23:53
 * @description ${Description}
 */
public interface ArticleTagService extends IService<ArticleTag>{


    int updateBatch(List<ArticleTag> list);

    int updateBatchSelective(List<ArticleTag> list);

    int batchInsert(List<ArticleTag> list);

    int insertOrUpdate(ArticleTag record);

    int insertOrUpdateSelective(ArticleTag record);

    IPage<ArticleTagBO> getPage(Page<ArticleTag> page, ArticleTagQuery query);
}
