/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 8.0.27 : Database - communication
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`communication` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `communication`;

/*Table structure for table `comm_mail_send_log` */

DROP TABLE IF EXISTS `comm_mail_send_log`;

CREATE TABLE `comm_mail_send_log` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '发送地址',
  `title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '标题',
  `message_temp_id` bigint DEFAULT NULL COMMENT '模板id',
  `content` varchar(800) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '内容',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `is_success` tinyint DEFAULT NULL COMMENT '0:失败，1：成功',
  PRIMARY KEY (`id`),
  UNIQUE KEY `un` (`address`,`send_time`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

/*Table structure for table `comm_message_temp` */

DROP TABLE IF EXISTS `comm_message_temp`;

CREATE TABLE `comm_message_temp` (
  `id` bigint NOT NULL,
  `type` tinyint DEFAULT NULL COMMENT '模板类型',
  `title` varchar(100) DEFAULT NULL COMMENT '模板标题',
  `content` varchar(800) DEFAULT NULL COMMENT '模板内容',
  `is_effective` tinyint DEFAULT NULL COMMENT '0:无效，1:有效',
  `create_time` datetime DEFAULT NULL COMMENT '创建事件',
  `update_time` datetime DEFAULT NULL COMMENT '修改事件',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

/*Table structure for table `comm_qqbot_send_log` */

DROP TABLE IF EXISTS `comm_qqbot_send_log`;

CREATE TABLE `comm_qqbot_send_log` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `bot_account` bigint DEFAULT NULL COMMENT '机器人账号',
  `target_account` bigint DEFAULT NULL COMMENT '目标账号',
  `message_temp_id` bigint DEFAULT NULL COMMENT '消息模板id',
  `content` varchar(500) DEFAULT NULL COMMENT '内容',
  `send_time` datetime DEFAULT NULL COMMENT '发送事件',
  `is_success` tinyint DEFAULT NULL COMMENT '0:失败，1：成功',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

CREATE TABLE `comm_bot` (
                            `id` bigint(11) NOT NULL AUTO_INCREMENT,
                            `remark` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci  DEFAULT NULL COMMENT '备注',
                            `account` bigint(20)  DEFAULT NULL COMMENT '账户',
                            `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci    DEFAULT NULL COMMENT '密码',
                            `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci   DEFAULT NULL COMMENT '昵称',
                            `level` int(4) DEFAULT NULL COMMENT '级别',
                            `is_online` tinyint(1) DEFAULT NULL COMMENT '0：未上线，1：上线',
                            `is_open` tinyint(1) DEFAULT NULL COMMENT '0:未使用，1：使用',
                            `create_time` datetime DEFAULT NULL,
                            `update_time` datetime DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

CREATE TABLE `comm_bot_function` (
                                     `id` bigint(11) NOT NULL AUTO_INCREMENT,
                                     `keyword` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci   DEFAULT NULL COMMENT '功能关键词',
                                     `describes` varchar(800) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci   DEFAULT NULL COMMENT '功能描述',
                                     `create_time` datetime DEFAULT NULL,
                                     `update_time` datetime DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

CREATE TABLE `comm_bot_use_function` (
                                         `id` bigint(11) NOT NULL AUTO_INCREMENT,
                                         `bit_id` bigint(11) DEFAULT NULL COMMENT '机器人id',
                                         `function_id` bigint(11) DEFAULT NULL COMMENT '功能id',
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

