![](https://static01.imgkr.com/temp/15d62f24286d4559a90598e90d88b9a0.png)
# 项目：communication（意义：交流）
开发者QQ：1491989462
> 简介：一个上能聊技术，下能聊人生的交流项目。

现阶段项目还没有具体的业务填充，等以后确定项目具体功能方向…。

# 环境

- JDK11
- MySQL5.7+
- MyBatisPlus
- SpringBoot
- SpringCloud
- nacos
- MapStruct
- Redis
- qq机器人
- Hutool
- …

因为功能未确定，所以技术先确定这些。

# 包结构

```tex
communication
	|
	--------communication-base
		|
		--------communication-common
	|
	--------communication-pom
	|
	--------communication-chat
	|
	--------communication-social
		|
		--------communication-social-core
	|
	--------communication-support
		|
		--------communication-support-mail
		    |
		    --------communication-support-mail-api
		    |
		    --------communication-support-mail-core
		    |
		--------communication-support-qqbot
		    |
		    --------communication-support-qqbot-api
		    |
		    --------communication-support-qqbot-core
		    |
		--------communication-support-branch
		    |
		    --------communication-support-branch-core
```

`communication`：顶级项目目录

`communication-base`：基础模块

`communication-common`：公共功能模块

`communication-pom`：项目所有依赖包

`communication-chat`：暂定的聊天模块

`communication-social`：暂定的社交模块

`communication-social-core`：社交核心代码模块

`communication-support`：项目功能支持模块

`communication-support-mail`：邮件支持模块

`communication-support-mail-api`：邮件对外提接口服务

`communication-support-mail-core`：邮件核心代码服务

`communication-support-qqbot`：qqbot服务支持

`communication-support-qqbot-api`：qqbot对外api模块

`communication-support-qqbot-core`：qqbot核心代码服务

`communication-support-branch`：commun项目分支功能模块

`communication-support-branch-core`：分支功能模块核心服务

…

# 端口规定

范围 8500-8599

- communication-social：8501
- communication-support-mail-core：8502
- communication-support-qqbot-core：8503
- communication-support-branch-core：8504
- …



# 现有功能

1. 项目基础环境搭建完成
2. 统一返回结果完成
3. 统一错误处理完成
4. 项目日志打印
5. 对象转换（converter）完成
6. 邮件发送，单个、批量
7. 统一Mapper扫描配置
8. qqbot机器人功能：同意好友申请、私聊回复、私聊消息，后续有功能再加
9. feign服务调用
10. 消息模板功能CRUD

基本项目架子算是搭建完成了，就等确定项目该有那些功能了。
```text
docker run -p 3306:3306 --name mysql -v /usr/local/mysql/conf:/etc/mysql/conf.d -v /usr/local/mysql/logs:/logs -v /usr/local/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root -d mysql:5.7
docker exec -it b5040e3f7927 /bin/bash
ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'root'
name=j3-baiqi
./startup.sh -m standalone
172.21.0.7
nohup java -Xms256m -Xmx256m  -jar qqbot-core.jar > ./logs/qqbot_core.log 2>&1 &
```