package cn.baiqi.qqbotapi.v1;

import cn.baiqi.common.constant.ServicesConstant;
import cn.baiqi.qqbotapi.dto.PrivateMessageDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotapi.v1
 * @createTime 2021/11/27 - 22:49
 * @description qqbot机器人私聊api
 */
@FeignClient(name = ServicesConstant.QQ_BOT)
@ResponseBody
@RequestMapping("/v1/qqbot")
public interface QqBotPrivate {

    /**
     * 判断 targetAccount 是否是 botAccount 的好友
     * @param botAccount
     * @param targetAccount
     * @return
     */
    @GetMapping("/isFriend")
    Boolean isFriend(@RequestParam(name = "botAccount") String botAccount, @RequestParam(name = "targetAccount") String targetAccount);

    /**
     * 私发消息
     * @param privateMessageDTO
     */
    @PostMapping("/sendMessage")
    void sendMessage(@RequestBody PrivateMessageDTO privateMessageDTO);

}
