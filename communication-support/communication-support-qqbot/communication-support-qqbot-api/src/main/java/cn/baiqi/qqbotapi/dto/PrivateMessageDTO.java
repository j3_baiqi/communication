package cn.baiqi.qqbotapi.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotapi.dto
 * @createTime 2021/11/27 - 22:55
 * @description 私聊消息
 */
@Accessors(chain = true)
@Data
public class PrivateMessageDTO {

    /**
     * 机器人账号，为空则默认机器人发送
     */
    private String botAccount;

    /**
     * 对方账号
     */
    private String targetAccount;

    /**
     * 内容
     */
    private String content;

    /**
     * 内容模板id，空直接发送内容反之套用模板
     */
    private Long tempId;

}
