package cn.baiqi.qqbotapi;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author J3-白起
 * @package cn.baiqi.mailapi
 * @createTime 2021/11/26 - 9:48
 * @description qqbot自动导入注解
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(EnableQqBotFeignClients.Config.class)
public @interface EnableQqBotFeignClients {
    @Configuration
    @EnableFeignClients(basePackageClasses = EnableQqBotFeignClients.class, basePackages = "cn.baiqi.qqbotapi.v1")
    class Config {
    }
}