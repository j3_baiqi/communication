package cn.baiqi.qqbotcore.bo;

import cn.baiqi.qqbotcore.po.Bot;
import lombok.Data;

/**
 * 
 * @TableName comm_bot
 */
@Data
public class BotBO extends Bot {
}