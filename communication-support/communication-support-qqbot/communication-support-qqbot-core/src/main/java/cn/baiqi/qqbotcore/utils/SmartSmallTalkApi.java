package cn.baiqi.qqbotcore.utils;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.utils
 * @createTime 2021/12/3 - 23:19
 * @description 智能闲聊api
 */
@Slf4j
public class SmartSmallTalkApi {

    private static final String SMART_URL = "http://i.xiaoi.com/robot/webrobot?&";

    private static Map<String, String> cookieMap = new ConcurrentHashMap<>();

    /**
     * 根据传入的消息，返回响应结果
     *
     * @param msg
     * @return
     */
    public static List<String> getResult(String msg) {
        // 执行 http
        HttpResponse execute = toHttp(msg);
        // 处理 cookies
        handleCookies(execute.getCookies());

//        log.info("=============handleCookies:{}", cookieMap);

        // 处理结果，并返回
        String body = execute.body();
        return handleBody(body);
    }


    private static List<String> handleBody(String body) {

        List<String> result = new ArrayList<>();

        log.info("stringList：{}", body);
        List<String> stringList = Arrays.asList(body.split(";"));
        log.info("stringList：{}", stringList.size());

        if (stringList.size() == 5) {
            String contentMsg = handleContentMsg(stringList.get(4));
            result.add(contentMsg);
//            log.info("content：{}", contentMsg);
        }

        if (stringList.size() == 6) {
            String dataMsg = handleDataMsg(stringList.get(4));
            String contentMsg = handleContentMsg(stringList.get(5));
            result.add(dataMsg);
            result.add(contentMsg);
//            log.info("data：{}", dataMsg);
//            log.info("content：{}", contentMsg);
        }

        if (stringList.size() == 1) {
            String contentMsg = handleContentMsg(stringList.get(0));
            result.add(contentMsg);
            log.info("content：{}", contentMsg);
        }

        if (stringList.size() == 2) {
            String dataMsg = handleDataMsg(stringList.get(0));
            String contentMsg = handleContentMsg(stringList.get(1));
            result.add(dataMsg);
            result.add(contentMsg);
        }
//        log.info("content：{}", "Hi，我是J3机器人，我可以查天气，讲笑话，订机票哦~ 除此之外还有几十项实用好玩的功能哦~ ");
        return result;
    }


    private static String handleContentMsg(String bodyMsg) {
        String replace = bodyMsg.replace("__webrobot_processMsg(", "");
        replace = replace.substring(0, replace.length() - 1);
        JSONObject body = (JSONObject) JSONUtil.parseObj(replace).get("body");
        String content = body.get("content").toString().replaceAll("小i机器人", "J3机器人");
        content = content.replaceAll("董事长袁辉", "开发者，J3_baiqi，回复：J3可联系到他哟~");
        if (null != content && content.contains("defaultReply")) {
            return "J3机器人还不太明白！，你可以联系我的开发者";
        }
        if (null != content && content.contains("下载Android客户")) {
            return "J3机器人还不太明白！，你可以联系我的开发者";
        }
        if (null != content && content.contains("联系方式")) {
            return "J3机器人还不太明白！，你可以联系我的开发者";
        }
        if (null != content && content.contains("i.xiaoi.com")) {
            return "J3机器人还不太明白！，你可以联系我的开发者";
        }
        if (null != content && content.contains("官网")) {
            return "J3机器人官方负责人：J3—白起，回复：J3可联系他哟~";
        }
        if (null != content && content.contains("公司的地址")) {
            return "J3机器人官方负责人：J3—白起，回复：J3可联系他哟~";
        }
        if (null != content && content.contains("成立于2001年")) {
            return "J3机器人就是你的专属机器人哦~";
        }
        if (null != content && content.contains("袁辉是我的老板")) {
            return "J3机器人没有老板，只有主人~";
        }
        if (null != content && content.contains("专属机器人")) {
            return "J3机器人最近才开发出来的~";
        }
        if (null != content && content.contains("MAX")) {
            return "当然是那个只会 CRUD 的 J3 啦！";
        }
        return content;
    }

    private static String handleDataMsg(String bodyMsg) {
        String replace = bodyMsg.replace("__webrobot_processMsg(", "");
        replace = replace.substring(0, replace.length() - 1);
        JSONObject body = (JSONObject) JSONUtil.parseObj(replace).get("body");
        return body.get("name") + "\r\n" + body.get("data");
    }

    private static void handleCookies(List<HttpCookie> cookies) {
        // 三方返回了两个值，重置cookies
        if (cookies.size() == 2) {
            for (HttpCookie cookie : cookies) {
                cookieMap.put(cookie.getName(), cookie.getValue());
            }
            return;
        }
        // 返回了一个，替换 nonce
        if (cookies.size() == 1) {
            for (HttpCookie cookie : cookies) {
                cookieMap.put(cookie.getName(), cookie.getValue());
            }
            return;
        }
    }

    private static HttpResponse toHttp(String msg) {
        JSONObject from = new JSONObject();
        from.put("callback", "__webrobot_processMsg");
        from.put("ts", System.currentTimeMillis());

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sessionId", "e62c993d8f904060883538e54d092961");
        jsonObject.put("robotId", "webbot");
        jsonObject.put("userId", "2d269b145eb04774bcdd1f82b07dfb54");
        jsonObject.put("type", "txt");
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("content", msg);
        jsonObject.put("body", jsonBody);

        from.put("data", jsonObject);

//        log.info("=====================getCookie():{}", getCookie());
        return HttpRequest.get(SMART_URL)
                .header("Cookie", getCookie())
                .form(from)
                .execute();
    }


    private static String getCookie() {
        if (cookieMap.size() == 0) {
            return "";
        }
        StringBuilder cookie = new StringBuilder();
        for (Map.Entry<String, String> stringStringEntry : cookieMap.entrySet()) {
            cookie.append(stringStringEntry.getKey()).append("=").append(stringStringEntry.getValue()).append(",");
        }
        return cookie.substring(0, cookie.toString().length() - 1);
    }


    public static void main(String[] args) throws InterruptedException {
        getResult("哈哈");
        Thread.sleep(2000);
        getResult("傻逼");
        Thread.sleep(2000);
        getResult("今日大事");
        Thread.sleep(2000);
        getResult("1");
        Thread.sleep(2000);
        getResult("2");
        Thread.sleep(2000);
        getResult("王祖贤");
        Thread.sleep(2000);
        getResult("你有什么功能");
    }

}
