package cn.baiqi.qqbotcore.converter;

import cn.baiqi.qqbotcore.api.v1.form.BotForm;
import cn.baiqi.qqbotcore.api.v1.form.BotFunctionForm;
import cn.baiqi.qqbotcore.api.v1.vo.BotFunctionVO;
import cn.baiqi.qqbotcore.api.v1.vo.BotVO;
import cn.baiqi.qqbotcore.bo.BotBO;
import cn.baiqi.qqbotcore.bo.BotFunctionBO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.converter
 * @createTime 2021/11/25 - 10:11
 * @description
 */
@Mapper(componentModel = "spring", typeConversionPolicy = ReportingPolicy.ERROR)//交给spring管理
public interface BotConverter {
    BotVO converter(BotBO botBO);

    BotBO converter(BotForm botForm);

    BotFunctionBO converter(BotFunctionForm botFunctionForm);

    BotFunctionVO converter(BotFunctionBO botFunctionBO);
}
