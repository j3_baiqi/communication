package cn.baiqi.qqbotcore.service;

import cn.baiqi.qqbotcore.api.v1.query.BotFunctionQuery;
import cn.baiqi.qqbotcore.bo.BotFunctionBO;
import cn.baiqi.qqbotcore.po.BotFunction;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface BotFunctionService extends IService<BotFunction> {

    IPage<BotFunctionBO> getPage(Page<BotFunction> page, BotFunctionQuery query);

    String getFunction(long accountCodeNumber, String text);

    String getFunction(String text);

    String getDefaultFunction();

    String getBlacklist( long accountCodeNumber, String keyword);
}
