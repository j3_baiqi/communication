package cn.baiqi.qqbotcore.mapper;

import cn.baiqi.qqbotcore.po.QqBotSendLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【comm_qqbot_send_log】的数据库操作Mapper
* @createDate 2021-11-27 18:56:15
* @Entity cn.baiqi.qqbotcore.po.QqBotSendLog
*/
public interface QqBotSendLogMapper extends BaseMapper<QqBotSendLog> {

}




