package cn.baiqi.qqbotcore.listener;

import cn.baiqi.common.constant.BotFunctionConstant;
import cn.baiqi.qqbotcore.service.BotFunctionService;
import cn.baiqi.qqbotcore.service.BotService;
import cn.hutool.core.util.RandomUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import love.forte.simbot.annotation.OnFriendAddRequest;
import love.forte.simbot.annotation.OnFriendIncrease;
import love.forte.simbot.api.message.events.FriendAddRequest;
import love.forte.simbot.api.message.events.FriendIncrease;
import love.forte.simbot.api.sender.Sender;
import love.forte.simbot.api.sender.Setter;
import org.springframework.stereotype.Component;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.listener
 * @createTime 2021/11/27 - 15:59
 * @description 监听好友添加事件
 */
@Slf4j
@Data
@AllArgsConstructor
@Component
public class MyOnFriendAddRequest {

    private final BotFunctionService botFunctionService;

    /**
     * 好友添加事件，一律通过
     *
     * @param friendAddRequest
     * @param setter
     */
    @OnFriendAddRequest
    public void onFriendAddRequest(FriendAddRequest friendAddRequest, Setter setter) {
        String s = RandomUtil.randomString(5);
        setter.setFriendAddRequest(friendAddRequest.getFlag(), "commun" + s, true, false);
    }

    /**
     * 通过后，打招呼
     *
     * @param friendIncrease
     * @param sender
     */
    @OnFriendIncrease
    public void FriendIncrease(FriendIncrease friendIncrease, Sender sender) {
        String function = botFunctionService.getFunction(friendIncrease.getBotInfo().getAccountCodeNumber(), BotFunctionConstant.SAY_HELLO);
        sender.sendPrivateMsg(friendIncrease.getAccountInfo(), function);
    }

}
