package cn.baiqi.qqbotcore.handler.smart;

import cn.baiqi.qqbotcore.handler.BotMsgHandler;
import cn.baiqi.qqbotcore.service.BotFunctionService;
import cn.baiqi.qqbotcore.utils.SmartSmallTalkApi;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import love.forte.simbot.api.message.MessageContent;
import love.forte.simbot.api.message.MessageContentBuilder;
import love.forte.simbot.api.message.MessageContentBuilderFactory;
import love.forte.simbot.api.message.events.MsgGet;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.handler
 * @createTime 2021/12/3 - 23:10
 * @description 智能消息处理
 */
@Slf4j
@Data
@AllArgsConstructor
@Component
public class SmartMsgHandler implements BotMsgHandler {

    private final MessageContentBuilderFactory builderFactory;
    private final BotFunctionService botFunctionService;

    @Override
    public MessageContent handlerMsg(MsgGet msgGet) {
        MessageContentBuilder builder = builderFactory.getMessageContentBuilder();
        // SmartSmallTalkApi
        List<String> stringList = SmartSmallTalkApi.getResult(msgGet.getText().trim());
        if (stringList.size() == 1) {
            return builder.text(stringList.get(0))
                    .build();
        }
        if (stringList.size() == 2) {
            return builder.text(stringList.get(0))
                    .text("\r\n")
                    .text(stringList.get(1))
                    .build();
        }
        return builder.text("J3机器人还不太明白！").build();
    }
}
