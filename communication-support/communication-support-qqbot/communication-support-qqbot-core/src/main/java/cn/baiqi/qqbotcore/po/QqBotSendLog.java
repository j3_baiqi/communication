package cn.baiqi.qqbotcore.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @TableName comm_qqbot_send_log
 */
@Accessors(chain = true)
@TableName(value ="comm_qqbot_send_log")
@Data
public class QqBotSendLog implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 机器人账号
     */
    private Long botAccount;

    /**
     * 目标账号
     */
    private Long targetAccount;

    /**
     * 消息模板id
     */
    private Long messageTempId;

    /**
     * 内容
     */
    private String content;

    /**
     * 发送事件
     */
    private LocalDateTime sendTime;

    /**
     * 0:失败，1：成功
     */
    @TableField(value = "is_success")
    private Boolean success;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}