package cn.baiqi.qqbotcore.service.impl;

import cn.baiqi.qqbotcore.api.v1.query.BotQuery;
import cn.baiqi.qqbotcore.bo.BotBO;
import cn.baiqi.qqbotcore.mapper.BotMapper;
import cn.baiqi.qqbotcore.po.Bot;
import cn.baiqi.qqbotcore.service.BotService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import love.forte.simbot.bot.BotManager;
import love.forte.simbot.bot.BotVerifyInfo;
import love.forte.simbot.bot.BotVerifyInfos;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 *
 */
@Slf4j
@Data
@AllArgsConstructor
@Service
public class BotServiceImpl extends ServiceImpl<BotMapper, Bot>
        implements BotService {

    private final BotMapper botMapper;
    private final BotManager botManager;

    @Override
    public IPage<BotBO> getPage(Page<Bot> page, BotQuery botQuery) {

        return botMapper.getPage(page, botQuery);
    }

    @Override
    public Boolean online(Long id) {
        Bot bot = getById(id);
        if (Objects.isNull(bot)) {
            return Boolean.FALSE;
        }
        BotVerifyInfo botVerifyInfo = BotVerifyInfos.getInstance(bot.getAccount().toString(), bot.getPassword());
        try {
            botManager.registerBot(botVerifyInfo);
        } catch (Exception e) {
            //错误处理
            log.error("====机器人登录失败：{}", e);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public Boolean offline(Long id) {
        Bot bot = getById(id);
        if (Objects.isNull(bot)) {
            return Boolean.FALSE;
        }
        try {
            botManager.destroyBot(bot.getAccount().toString());
        } catch (Exception e) {
            //错误处理
            log.error("====机器人注销失败：{}", e);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}




