package cn.baiqi.qqbotcore.listener;

import cn.baiqi.common.constant.BotFunctionConstant;
import cn.baiqi.qqbotcore.service.BotFunctionService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import love.forte.simbot.annotation.OnGroupAddRequest;
import love.forte.simbot.annotation.OnGroupMemberIncrease;
import love.forte.simbot.api.message.MessageContent;
import love.forte.simbot.api.message.MessageContentBuilder;
import love.forte.simbot.api.message.MessageContentBuilderFactory;
import love.forte.simbot.api.message.containers.AccountInfo;
import love.forte.simbot.api.message.containers.GroupInfo;
import love.forte.simbot.api.message.events.GroupAddRequest;
import love.forte.simbot.api.message.events.GroupMemberIncrease;
import love.forte.simbot.api.sender.Sender;
import love.forte.simbot.api.sender.Setter;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.listener.privates
 * @createTime 2021/12/2 - 15:26
 * @description
 */
@Slf4j
@Data
@AllArgsConstructor
@Component
public class MyOnGroupAddRequest {


    private final BotFunctionService botFunctionService;
    private final MessageContentBuilderFactory builderFactory;

    /**
     * 添加事件，一律通过
     *
     * @param groupAddRequest
     * @param setter
     */
    @OnGroupAddRequest
    public void onFriendAddRequest(GroupAddRequest groupAddRequest, Setter setter) {
        setter.setGroupAddRequestAsync(groupAddRequest.getFlag(), true, false, "");
    }

    /**
     * 群成员增加时触发
     *
     * @param
     * @param
     */
    @OnGroupMemberIncrease
    public void onGroupMemberIncrease(GroupMemberIncrease groupMemberIncrease, Sender sender) {
        GroupInfo groupInfo = groupMemberIncrease.getGroupInfo();
        log.info("=======groupInfo:{}", groupInfo);
        AccountInfo accountInfo = groupMemberIncrease.getAccountInfo();
        log.info("=======accountInfo:{}", accountInfo);

        // 查一下功能黑名单
        String blacklist = botFunctionService.getBlacklist(groupMemberIncrease.getBotInfo().getAccountCodeNumber(), BotFunctionConstant.NEW_PEOPLE_ADD_GROUP);
        if (Objects.isNull(blacklist) || blacklist.contains(groupInfo.getGroupCode())){
            return;
        }

        MessageContentBuilder messageContentBuilder = builderFactory.getMessageContentBuilder();

        String function = botFunctionService.getFunction(BotFunctionConstant.NEW_PEOPLE_ADD_GROUP);
        String groupName = groupInfo.getGroupName();
        if (Objects.isNull(groupName) || groupName.trim().isEmpty()) {
            groupName = "秘密";
        }
        String accountCode = accountInfo.getAccountCode();
        if (Objects.isNull(accountCode) || accountCode.trim().isEmpty()) {
            accountCode = "秘密";
        }
        String accountRemarkOrNickname = accountInfo.getAccountRemarkOrNickname();
        if (Objects.isNull(accountRemarkOrNickname) || accountRemarkOrNickname.trim().isEmpty()) {
            accountRemarkOrNickname = "秘密";
        }
        String format = String.format(function, groupName, accountCode, accountRemarkOrNickname);
        log.info("===========================format:{}", format);

        MessageContent build = messageContentBuilder.atAll()
                .text("\n")
                .text(format)
                .build();
        sender.sendGroupMsgAsync(groupInfo, build);
    }
}
