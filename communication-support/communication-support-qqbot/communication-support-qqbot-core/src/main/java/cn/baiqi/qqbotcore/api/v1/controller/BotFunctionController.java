package cn.baiqi.qqbotcore.api.v1.controller;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.api.v1.controller
 * @createTime 2021/12/1 - 15:50
 * @description
 */

import cn.baiqi.common.annotation.ResponseResult;
import cn.baiqi.qqbotcore.api.v1.form.BotForm;
import cn.baiqi.qqbotcore.api.v1.form.BotFunctionForm;
import cn.baiqi.qqbotcore.api.v1.query.BotFunctionQuery;
import cn.baiqi.qqbotcore.api.v1.query.BotQuery;
import cn.baiqi.qqbotcore.api.v1.vo.BotFunctionVO;
import cn.baiqi.qqbotcore.api.v1.vo.BotVO;
import cn.baiqi.qqbotcore.converter.BotConverter;
import cn.baiqi.qqbotcore.po.Bot;
import cn.baiqi.qqbotcore.po.BotFunction;
import cn.baiqi.qqbotcore.service.BotFunctionService;
import cn.baiqi.qqbotcore.service.BotService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@AllArgsConstructor
@ResponseResult
@RequestMapping("/botFunction")
public class BotFunctionController {

    private final BotFunctionService service;
    private final BotConverter converter;

    @GetMapping("")
    public IPage<BotFunctionVO> getPage(Page<BotFunction> page, BotFunctionQuery query) {
        var iPage = service.getPage(page, query);
        return iPage.convert(converter::converter);
    }

    @PostMapping("")
    public void save(@RequestBody @Validated BotFunctionForm form) {
        service.save(converter.converter(form));
    }

    @PostMapping("/update")
    public void update(@RequestBody @Validated BotFunctionForm form) {
        service.updateById(form);
    }

    @GetMapping("/delete")
    public void delete(@RequestParam(name = "id") Long id) {
        service.removeById(id);
    }

}
