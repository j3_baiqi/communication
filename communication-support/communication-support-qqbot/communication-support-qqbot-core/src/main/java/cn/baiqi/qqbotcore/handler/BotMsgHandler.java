package cn.baiqi.qqbotcore.handler;

import love.forte.simbot.api.message.MessageContent;
import love.forte.simbot.api.message.events.MessageGet;
import love.forte.simbot.api.message.events.MsgGet;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.handler
 * @createTime 2021/12/1 - 18:00
 * @description
 */
public interface BotMsgHandler {

    MessageContent handlerMsg(MsgGet msgGet);
}
