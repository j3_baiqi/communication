package cn.baiqi.qqbotcore.api.v1.vo;

import cn.baiqi.qqbotcore.po.Bot;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName comm_bot
 */
@Data
public class BotVO extends Bot {
}