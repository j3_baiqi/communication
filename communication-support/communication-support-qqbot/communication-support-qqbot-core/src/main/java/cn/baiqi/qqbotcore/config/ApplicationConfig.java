package cn.baiqi.qqbotcore.config;

import love.forte.simbot.spring.autoconfigure.EnableSimbot;
import org.springframework.context.annotation.Configuration;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.config
 * @createTime 2021/11/27 - 1:15
 * @description
 */
@EnableSimbot // 开启机器人组件功能
@Configuration
public class ApplicationConfig {
}
