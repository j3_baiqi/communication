package cn.baiqi.qqbotcore.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.service
 * @createTime 2021/11/27 - 17:01
 * @description 发送QQ信息
 */
public interface PrivateSendQQMessage {

    /**
     * 随机机器人发送信息
     * @param account
     * @param content
     * @return
     */
    Boolean sendMessage(String account, String content);

    default Map<String, Boolean> batchSendMessage(List<String> accountList, String content) {
        Map<String, Boolean> result = new ConcurrentHashMap<>(accountList.size());
        accountList.forEach(account -> result.put(account, sendMessage(account, content)));
        return result;
    }

    /**
     * 指定机器人发送信息
     * @param account
     * @param content
     * @return
     */
    Boolean sendMessage(String botAccount, String account, String content);

    default Map<String, Boolean> batchSendMessage(String botAccount, List<String> accountList, String content) {
        Map<String, Boolean> result = new ConcurrentHashMap<>(accountList.size());
        accountList.forEach(account -> result.put(account, sendMessage(botAccount, account, content)));
        return result;
    }


}
