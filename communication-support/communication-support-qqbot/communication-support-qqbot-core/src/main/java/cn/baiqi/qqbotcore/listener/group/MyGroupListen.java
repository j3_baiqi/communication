package cn.baiqi.qqbotcore.listener.group;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.listener
 * @createTime 2021/11/27 - 1:16
 * @description
 */

import cn.baiqi.qqbotcore.handler.group.GroupMsgHandler;
import cn.baiqi.qqbotcore.handler.smart.SmartMsgHandler;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import love.forte.simbot.annotation.Filter;
import love.forte.simbot.annotation.OnGroup;
import love.forte.simbot.api.message.MessageContent;
import love.forte.simbot.api.message.events.GroupMsg;
import love.forte.simbot.api.sender.MsgSender;
import org.springframework.stereotype.Component;

@Slf4j
@Data
@AllArgsConstructor
@Component
public class MyGroupListen {

    private final GroupMsgHandler groupMsgHandler;
    private final SmartMsgHandler smartMsgHandler;

    @OnGroup
    @Filter(atBot = true)
    public void replyGroupMsg(GroupMsg groupMsg, MsgSender msgSender) {
        log.info("==================OnGroup监听到消息~，{}", groupMsg);
        // MessageContent messageContent = groupMsgHandler.handlerMsg(groupMsg);
        if (groupMsg.getText().trim().equals("J3")) {
            String msg = "下面联系我：\n" +
                    "QQ：1491989462\n" +
                    "Email：j3_liuliang@163.com\n" +
                    "CSDN：https://blog.csdn.net/qq_40399646";
            msgSender.SENDER.sendGroupMsg(groupMsg, msg);
            return;
        }
        if (groupMsg.getText().trim().contains("妈妈")) {
            String msg = "她很神秘，但主人都叫她：憨憨";
            msgSender.SENDER.sendGroupMsg(groupMsg, msg);
            return;
        }
        MessageContent messageContent = smartMsgHandler.handlerMsg(groupMsg);
        msgSender.SENDER.sendGroupMsg(groupMsg.getGroupInfo(), messageContent);
    }

}
