package cn.baiqi.qqbotcore.mapper;

import cn.baiqi.qqbotcore.api.v1.query.BotFunctionQuery;
import cn.baiqi.qqbotcore.bo.BotFunctionBO;
import cn.baiqi.qqbotcore.po.BotFunction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity cn.baiqi.qqbotcore.po.BotFunction
 */
public interface BotFunctionMapper extends BaseMapper<BotFunction> {

    IPage<BotFunctionBO> getPage(@Param("page") Page<BotFunction> page, @Param("query") BotFunctionQuery query);

    String getFunction(@Param("accountCodeNumber") long accountCodeNumber, @Param("text") String text);

    String getBlacklist(@Param("accountCodeNumber") long accountCodeNumber, @Param("keyword") String keyword);
}




