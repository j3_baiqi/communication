package cn.baiqi.qqbotcore.bo;

import cn.baiqi.qqbotcore.po.BotFunction;
import lombok.Data;

/**
 * 
 * @TableName comm_bot_function
 */
@Data
public class BotFunctionBO extends BotFunction {
}