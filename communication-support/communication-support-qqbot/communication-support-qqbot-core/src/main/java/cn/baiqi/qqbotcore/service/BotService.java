package cn.baiqi.qqbotcore.service;

import cn.baiqi.qqbotcore.api.v1.query.BotQuery;
import cn.baiqi.qqbotcore.api.v1.vo.BotVO;
import cn.baiqi.qqbotcore.bo.BotBO;
import cn.baiqi.qqbotcore.po.Bot;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface BotService extends IService<Bot> {

    IPage<BotBO> getPage(Page<Bot> page, BotQuery botQuery);

    Boolean online(Long id);

    Boolean offline(Long id);
}
