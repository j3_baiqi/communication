package cn.baiqi.qqbotcore.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName comm_bot
 */
@TableName(value ="comm_bot")
@Data
public class Bot implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 备注
     */
    private String remark;

    /**
     * 账户
     */
    private Long account;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 级别
     */
    private Integer level;

    /**
     * 0：未上线，1：上线
     */
    @TableField(value = "is_online")
    private Boolean online;

    /**
     * 0:未使用，1：使用
     */
    @TableField(value = "is_open")
    private Boolean open;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}