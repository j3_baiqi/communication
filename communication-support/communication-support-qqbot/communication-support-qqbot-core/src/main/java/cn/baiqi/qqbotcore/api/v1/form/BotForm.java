package cn.baiqi.qqbotcore.api.v1.form;

import cn.baiqi.qqbotcore.po.Bot;
import lombok.Data;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.api.v1.form
 * @createTime 2021/12/1 - 16:05
 * @description
 */
@Data
public class BotForm extends Bot {
}
