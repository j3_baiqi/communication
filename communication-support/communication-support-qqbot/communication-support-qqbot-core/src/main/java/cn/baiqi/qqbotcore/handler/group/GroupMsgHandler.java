package cn.baiqi.qqbotcore.handler.group;

import cn.baiqi.common.constant.BotFunctionConstant;
import cn.baiqi.qqbotcore.handler.BotMsgHandler;
import cn.baiqi.qqbotcore.service.BotFunctionService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import love.forte.simbot.api.message.MessageContent;
import love.forte.simbot.api.message.MessageContentBuilder;
import love.forte.simbot.api.message.MessageContentBuilderFactory;
import love.forte.simbot.api.message.containers.BotInfo;
import love.forte.simbot.api.message.events.MsgGet;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.handler.group
 * @createTime 2021/12/2 - 15:05
 * @description
 */
@Slf4j
@Data
@AllArgsConstructor
@Component
public class GroupMsgHandler implements BotMsgHandler {

    private final MessageContentBuilderFactory builderFactory;
    private final BotFunctionService botFunctionService;

    @Override
    public MessageContent handlerMsg(MsgGet msgGet) {
        // 消息构建器：https://www.yuque.com/simpler-robot/simpler-robot-doc/hcf9gq
        MessageContentBuilder builder = builderFactory.getMessageContentBuilder();
        BotInfo botInfo = msgGet.getBotInfo();
        String text = msgGet.getText().trim();
        // 1.查询所有匹配该消息的对应机器人功能
        String content = botFunctionService.getFunction(botInfo.getAccountCodeNumber(), text);
        if (Objects.isNull(content)) {
            // 去拿一个默认返回消息
            content = botFunctionService.getDefaultFunction();
            content = botFunctionService.getFunction(botInfo.getAccountCodeNumber(), BotFunctionConstant.GROUP_DEFAULT_RETURN);
            if(Objects.isNull(content)){
                content = BotFunctionConstant.EMPTY;
            }
        }
        builder.text(content);
        return builder.build();
    }
}
