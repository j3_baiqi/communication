package cn.baiqi.qqbotcore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QqBotCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(QqBotCoreApplication.class, args);
    }

}
