package cn.baiqi.qqbotcore.service.impl;

import cn.baiqi.qqbotcore.mapper.QqBotSendLogMapper;
import cn.baiqi.qqbotcore.po.QqBotSendLog;
import cn.baiqi.qqbotcore.service.QqBotSendLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * @author Administrator
 * @description 针对表【comm_qqbot_send_log】的数据库操作Service实现
 * @createDate 2021-11-27 18:56:15
 */
@Slf4j
@Data
@AllArgsConstructor
@Service
public class QqBotSendLogServiceImpl extends ServiceImpl<QqBotSendLogMapper, QqBotSendLog>
        implements QqBotSendLogService {

    private final QqBotSendLogMapper qqBotSendLogMapper;

    @Override
    public void insert(String botAccount, String account, String content, boolean success) {
        QqBotSendLog qqBotSendLog = new QqBotSendLog()
                .setBotAccount(Long.parseLong(botAccount))
                .setTargetAccount(Long.parseLong(account))
                .setContent(content)
                .setSendTime(LocalDateTime.now())
                .setSuccess(success);
        save(qqBotSendLog);
    }
}




