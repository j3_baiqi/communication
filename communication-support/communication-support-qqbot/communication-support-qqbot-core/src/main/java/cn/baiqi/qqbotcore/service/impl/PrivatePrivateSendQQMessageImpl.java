package cn.baiqi.qqbotcore.service.impl;

import cn.baiqi.common.exception.SysException;
import cn.baiqi.common.utils.AsynchronousExecuteUtil;
import cn.baiqi.qqbotcore.service.PrivateSendQQMessage;
import cn.baiqi.qqbotcore.service.QqBotSendLogService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import love.forte.simbot.bot.Bot;
import love.forte.simbot.bot.BotManager;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.service.impl
 * @createTime 2021/11/27 - 17:02
 * @description
 */
@Slf4j
@Data
@AllArgsConstructor
@Service
public class PrivatePrivateSendQQMessageImpl implements PrivateSendQQMessage {

    private final BotManager botManager;
    private final QqBotSendLogService qqBotSendLogService;

    @Override
    public Boolean sendMessage(String account, String content) {
        boolean success = true;
        Bot defaultBot = null;

        try {
            defaultBot = botManager.getDefaultBot();
            defaultBot.getSender().SENDER.sendPrivateMsg(account, content);
        } catch (Exception e) {
            //错误处理
            log.error("发送QQ消息失败！，{}", e);
            success = false;
        }
        // 日志处理
        boolean finalSuccess = success;
        Bot finalDefaultBot = defaultBot;
        AsynchronousExecuteUtil.execute(() -> {
            qqBotSendLogService.insert(finalDefaultBot.getBotInfo().getAccountCode(), account, content, finalSuccess);
        });
        return success;
    }

    @Override
    public Boolean sendMessage(String botAccount, String account, String content) {
        boolean success = true;
        Bot botOrNull = null;
        try {
            botOrNull = botManager.getBotOrNull(botAccount);
            if (Objects.isNull(botOrNull)) {
                throw new SysException("机器人账号找不到！");
            }
            botOrNull.getSender().SENDER.sendPrivateMsg(account, content);
        } catch (Exception e) {
            //错误处理
            log.error("发送QQ消息失败！，{}", e);
            success = false;
        }
        // 日志处理
        boolean finalSuccess = success;
        AsynchronousExecuteUtil.execute(() -> {
            qqBotSendLogService.insert(botAccount, account, content, finalSuccess);
        });
        return success;
    }
}


