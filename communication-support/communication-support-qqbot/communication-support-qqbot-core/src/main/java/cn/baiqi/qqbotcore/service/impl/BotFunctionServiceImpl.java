package cn.baiqi.qqbotcore.service.impl;

import cn.baiqi.common.constant.BotFunctionConstant;
import cn.baiqi.qqbotcore.api.v1.query.BotFunctionQuery;
import cn.baiqi.qqbotcore.bo.BotFunctionBO;
import cn.baiqi.qqbotcore.mapper.BotFunctionMapper;
import cn.baiqi.qqbotcore.po.BotFunction;
import cn.baiqi.qqbotcore.service.BotFunctionService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 *
 */
@Slf4j
@Data
@AllArgsConstructor
@Service
public class BotFunctionServiceImpl extends ServiceImpl<BotFunctionMapper, BotFunction>
    implements BotFunctionService{

    private final BotFunctionMapper botFunctionMapper;

    @Override
    public IPage<BotFunctionBO> getPage(Page<BotFunction> page, BotFunctionQuery query) {
        return botFunctionMapper.getPage(page, query);
    }

    @Override
    public String getFunction(long accountCodeNumber, String text) {
        return botFunctionMapper.getFunction(accountCodeNumber, text);
    }

    @Override
    public String getFunction(String text) {
        final var lambdaQuery = lambdaQuery();
        lambdaQuery.eq(BotFunction::getKeyword, text);
        return lambdaQuery.one().getDescribes();
    }

    @Override
    public String getDefaultFunction() {
        final var lambdaQuery = lambdaQuery();
        lambdaQuery.eq(BotFunction::getKeyword, BotFunctionConstant.PRIVATE_DEFAULT_RETURN);
        return lambdaQuery.one().getDescribes();
    }

    @Override
    public String getBlacklist(long accountCodeNumber, String keyword) {
        return botFunctionMapper.getBlacklist(accountCodeNumber, keyword);
    }
}




