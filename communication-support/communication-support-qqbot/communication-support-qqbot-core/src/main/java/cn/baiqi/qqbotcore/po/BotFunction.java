package cn.baiqi.qqbotcore.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName comm_bot_function
 */
@TableName(value ="comm_bot_function")
@Data
public class BotFunction implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 功能关键词
     */
    private String keyword;

    /**
     * 功能描述
     */
    private String describes;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}