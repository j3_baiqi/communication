package cn.baiqi.qqbotcore.service;

import cn.baiqi.qqbotcore.po.QqBotSendLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【comm_qqbot_send_log】的数据库操作Service
* @createDate 2021-11-27 18:56:15
*/
public interface QqBotSendLogService extends IService<QqBotSendLog> {
    void insert(String accountCode, String account, String content, boolean success);
}
