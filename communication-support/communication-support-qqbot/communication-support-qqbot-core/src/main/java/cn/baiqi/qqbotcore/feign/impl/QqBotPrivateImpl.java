package cn.baiqi.qqbotcore.feign.impl;

import cn.baiqi.qqbotapi.dto.PrivateMessageDTO;
import cn.baiqi.qqbotapi.v1.QqBotPrivate;
import cn.baiqi.qqbotcore.service.PrivateSendQQMessage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.feign.impl
 * @createTime 2021/11/27 - 23:00
 * @description
 */
@Slf4j
@Component
@Data
@AllArgsConstructor
public class QqBotPrivateImpl implements QqBotPrivate {

    private final PrivateSendQQMessage privateSendQQMessage;

    @Override
    public Boolean isFriend(String botAccount, String targetAccount) {
        return null;
    }

    @Override
    public void sendMessage(PrivateMessageDTO privateMessageDTO) {
        if (Objects.isNull(privateMessageDTO.getBotAccount())) {
            privateSendQQMessage.sendMessage(privateMessageDTO.getTargetAccount()
                    , privateMessageDTO.getContent());
            return;
        }
        privateSendQQMessage.sendMessage(privateMessageDTO.getBotAccount()
                , privateMessageDTO.getTargetAccount()
                , privateMessageDTO.getContent());
    }
}
