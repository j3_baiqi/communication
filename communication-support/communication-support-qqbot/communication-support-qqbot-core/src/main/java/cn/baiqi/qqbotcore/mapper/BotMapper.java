package cn.baiqi.qqbotcore.mapper;

import cn.baiqi.qqbotcore.api.v1.query.BotQuery;
import cn.baiqi.qqbotcore.bo.BotBO;
import cn.baiqi.qqbotcore.po.Bot;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity cn.baiqi.qqbotcore.po.Bot
 */
public interface BotMapper extends BaseMapper<Bot> {

    IPage<BotBO> getPage(@Param("page") Page<Bot> page, @Param("query") BotQuery botQuery);
}




