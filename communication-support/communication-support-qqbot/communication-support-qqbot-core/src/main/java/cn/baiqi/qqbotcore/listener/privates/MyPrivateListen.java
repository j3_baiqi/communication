package cn.baiqi.qqbotcore.listener.privates;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.listener
 * @createTime 2021/11/27 - 1:16
 * @description
 */

import cn.baiqi.qqbotcore.handler.privates.PrivateMsgHandler;
import cn.baiqi.qqbotcore.handler.smart.SmartMsgHandler;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import love.forte.simbot.annotation.OnPrivate;
import love.forte.simbot.api.message.MessageContent;
import love.forte.simbot.api.message.events.PrivateMsg;
import love.forte.simbot.api.sender.MsgSender;
import org.springframework.stereotype.Component;

@Slf4j
@Data
@AllArgsConstructor
@Component
public class MyPrivateListen {

    private final PrivateMsgHandler privateMsgHandler;
    private final SmartMsgHandler smartMsgHandler;

    @OnPrivate()
    public void replyPrivateMsg(PrivateMsg privateMsg, MsgSender msgSender) {
        log.info("===========================================OnPrivate监听到消息~，类型：{}", privateMsg.getPrivateMsgType());
        if (privateMsg.getPrivateMsgType() == PrivateMsg.Type.FRIEND) {
            log.info("===========================================我监听到朋友的私聊消息~");
            if (privateMsg.getText().trim().equals("J3")) {
                String msg = "下面联系我：\n" +
                        "QQ：1491989462\n" +
                        "Email：j3_liuliang@163.com\n" +
                        "CSDN：https://blog.csdn.net/qq_40399646";
                msgSender.SENDER.sendPrivateMsg(privateMsg, msg);
                return;
            }
            if (privateMsg.getText().trim().contains("妈妈")) {
                String msg = "她很神秘，但主人都叫她：憨憨";
                msgSender.SENDER.sendPrivateMsg(privateMsg, msg);
                return;
            }
            // MessageContent messageContent = privateMsgHandler.handlerMsg(privateMsg);
            MessageContent messageContent = smartMsgHandler.handlerMsg(privateMsg);
            msgSender.SENDER.sendPrivateMsg(privateMsg, messageContent);
        }
    }

}
