package cn.baiqi.qqbotcore.api.v1.controller;

/**
 * @author J3-白起
 * @package cn.baiqi.qqbotcore.api.v1.controller
 * @createTime 2021/12/1 - 15:50
 * @description
 */

import cn.baiqi.common.annotation.ResponseResult;
import cn.baiqi.qqbotcore.api.v1.form.BotForm;
import cn.baiqi.qqbotcore.api.v1.query.BotQuery;
import cn.baiqi.qqbotcore.api.v1.vo.BotVO;
import cn.baiqi.qqbotcore.converter.BotConverter;
import cn.baiqi.qqbotcore.po.Bot;
import cn.baiqi.qqbotcore.service.BotService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@AllArgsConstructor
@ResponseResult
@RequestMapping("/bot")
public class BotController {

    private final BotService service;
    private final BotConverter converter;

    @GetMapping("")
    public IPage<BotVO> getPage(Page<Bot> page, BotQuery botQuery) {
        var iPage = service.getPage(page, botQuery);
        return iPage.convert(converter::converter);
    }

    @PostMapping("")
    public void save(@RequestBody @Validated BotForm botForm) {
        service.save(converter.converter(botForm));
    }

    @PostMapping("/update")
    public void update(@RequestBody @Validated BotForm botForm) {
        service.updateById(botForm);
    }

    @GetMapping("/delete")
    public void delete(@RequestParam(name = "id") Long id) {
        service.removeById(id);
    }

    @GetMapping("/online")
    public Boolean online(@RequestParam(name = "id") Long id){
        return service.online(id);
    }

    @GetMapping("/offline")
    public Boolean offline(@RequestParam(name = "id") Long id){
        return service.offline(id);
    }
}
