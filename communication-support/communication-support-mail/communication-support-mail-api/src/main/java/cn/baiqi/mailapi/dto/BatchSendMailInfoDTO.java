package cn.baiqi.mailapi.dto;

import cn.baiqi.common.entity.SendMailInfo;
import lombok.Data;

import java.util.List;

/**
 * @author J3-白起
 * @package cn.baiqi.mailapi.dto
 * @createTime 2021/11/25 - 16:08
 * @description
 */
@Data
public class BatchSendMailInfoDTO extends SendMailInfo {
    private List<String> addressList;
}
