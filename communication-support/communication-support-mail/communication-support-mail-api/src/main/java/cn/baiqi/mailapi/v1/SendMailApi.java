package cn.baiqi.mailapi.v1;

import cn.baiqi.common.constant.ServicesConstant;
import cn.baiqi.mailapi.dto.BatchSendMailInfoDTO;
import cn.baiqi.mailapi.dto.SendMailInfoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author J3-白起
 * @package cn.baiqi.mailapi.v1
 * @createTime 2021/11/25 - 16:03
 * @description
 */
@FeignClient(name = ServicesConstant.MAIL)
@ResponseBody
@RequestMapping("/v1/mail")
public interface SendMailApi {

    /**
     * 单个账户发送邮件
     *
     * @param sendMailInfoDTO
     * @return
     */
    @PostMapping("/send")
    Boolean sendMail(@RequestBody SendMailInfoDTO sendMailInfoDTO);

    /**
     * 批量发送，不同账户，不同内容
     * @param sendMailInfoDTOList
     * @return
     */
    default Boolean sendMailList(List<SendMailInfoDTO> sendMailInfoDTOList) {
        for (SendMailInfoDTO sendMailInfoDTO : sendMailInfoDTOList) {
            sendMail(sendMailInfoDTO);
        }
        return true;
    }

    /**
     * 批量账户发送邮件，同内容，不同账户
     *
     * @param batchSendMailInfoDTO
     * @return
     */
    @PostMapping("/sendBatch")
    Boolean sendMail(@RequestBody BatchSendMailInfoDTO batchSendMailInfoDTO);

}
