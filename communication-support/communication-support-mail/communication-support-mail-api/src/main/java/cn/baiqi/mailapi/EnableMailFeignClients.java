package cn.baiqi.mailapi;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author J3-白起
 * @package cn.baiqi.mailapi
 * @createTime 2021/11/26 - 9:48
 * @description 邮件feign自动导入注解
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(EnableMailFeignClients.Config.class)
public @interface EnableMailFeignClients {
    @Configuration
    @EnableFeignClients(basePackageClasses = EnableMailFeignClients.class, basePackages = "cn.baiqi.mailapi.v1")
    class Config {
    }
}
