package cn.baiqi.mailcore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MailCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(MailCoreApplication.class, args);
    }

}
