package cn.baiqi.mailcore.api.v1.controller;

import cn.baiqi.common.annotation.ResponseResult;
import cn.baiqi.common.utils.SendMailUtil;
import cn.baiqi.mailapi.dto.BatchSendMailInfoDTO;
import cn.baiqi.mailapi.v1.SendMailApi;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.v1.controller
 * @createTime 2021/11/25 - 17:21
 * @description
 */
@Slf4j
@AllArgsConstructor
@ResponseResult
@RequestMapping("/test/mail")
public class MailTestController {

    private final SendMailApi sendMailApi;


    @GetMapping("")
    public String mail(@RequestParam(name = "content", required = true) String content) {
        String targetMail01 = "1491989462@qq.com";
        String targetMail02 = "j3_liuliang@163.com";
        List<String> targetMailList = List.of(targetMail01, targetMail02);
        BatchSendMailInfoDTO sendMailInfoDTO = new BatchSendMailInfoDTO();
        sendMailInfoDTO.setAddressList(targetMailList);
        sendMailInfoDTO.setTitle("邮件服务");
        sendMailInfoDTO.setContent(content);
        sendMailInfoDTO.setIsHtml(false);
        sendMailApi.sendMail(sendMailInfoDTO);
        return "success";
    }

    @GetMapping("/sendError")
    public void sendError() {
        try {
            int i = 1 / 0;
        } catch (Exception e) {
            //错误处理
            SendMailUtil.send("测试", e, List.of(SendMailUtil.target01));
        }
    }

}
