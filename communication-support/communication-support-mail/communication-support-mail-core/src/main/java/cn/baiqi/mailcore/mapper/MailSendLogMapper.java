package cn.baiqi.mailcore.mapper;

import cn.baiqi.mailcore.po.MailSendLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity cn.baiqi.mailcore.po.MailSendLog
 */
public interface MailSendLogMapper extends BaseMapper<MailSendLog> {

    /**
     * 直接插入一个list，唯一键重复则直接更新
     * @param mailSendLogList
     */
    void insertList(@Param("list") List<MailSendLog> mailSendLogList);
}




