package cn.baiqi.mailcore.service.impl;

import cn.baiqi.mailcore.mapper.MailSendLogMapper;
import cn.baiqi.mailcore.po.MailSendLog;
import cn.baiqi.mailcore.service.MailSendLogService;
import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Slf4j
@Data
@Service
@RequiredArgsConstructor
public class MailSendLogServiceImpl extends ServiceImpl<MailSendLogMapper, MailSendLog>
        implements MailSendLogService {
    private final MailSendLogMapper mailSendLogMapper;

    @Override
    public void insertList(List<MailSendLog> mailSendLogList) {
        if (CollectionUtil.isEmpty(mailSendLogList)) {
            return;
        }
        mailSendLogMapper.insertList(mailSendLogList);
    }
}




