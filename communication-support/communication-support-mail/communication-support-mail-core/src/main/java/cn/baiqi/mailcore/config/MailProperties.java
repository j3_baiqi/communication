package cn.baiqi.mailcore.config;

import cn.hutool.extra.mail.MailAccount;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author J3-白起
 * @package cn.baiqi.tmailcore.config
 * @createTime 2021/11/25 - 17:29
 * @description 邮件配置
 */
@Data
@Slf4j
@Component
@EnableConfigurationProperties({MailProperties.class})
@ConfigurationProperties(MailProperties.PREFIX)
public class MailProperties {

    public static final String PREFIX = "comm.mail.config";

    public final MailAccount mailAccount = new MailAccount();
    /**
     * 邮件服务器的SMTP地址，可选，默认为smtp.<发件人邮箱后缀>
     */
    private String host;
    /**
     * 邮件服务器的SMTP端口，可选，默认25
     */
    private Integer port;
    /**
     * 发件人（必须正确，否则发送失败）
     */
    private String from;
    /**
     * 用户名，默认为发件人邮箱前缀
     */
    private String user;
    /**
     * 密码（注意，某些邮箱需要为SMTP服务单独设置授权码，详情查看相关帮助）EXWLACKYKALEWMBR
     */
    private String pass;

    private Boolean auth;

    public void setHost(String host) {
        this.host = host;
        mailAccount.setHost(this.host);
    }

    public void setPort(Integer port) {
        this.port = port;
        mailAccount.setPort(this.port);
    }

    public void setFrom(String from) {
        this.from = from;
        mailAccount.setFrom(this.from);
    }

    public void setUser(String user) {
        this.user = user;
        mailAccount.setUser(this.user);
    }

    public void setPass(String pass) {
        this.pass = pass;
        mailAccount.setPass(this.pass);
    }

    public void setAuth(Boolean auth) {
        this.auth = auth;
        mailAccount.setAuth(this.auth);
    }

/*
# 邮件服务器的SMTP地址，可选，默认为smtp.<发件人邮箱后缀>
host = smtp.163.com
# 邮件服务器的SMTP端口，可选，默认25
port = 25
# 发件人（必须正确，否则发送失败）
from = j3_liuliang@163.com
# 用户名，默认为发件人邮箱前缀
user = j3_baiqi
# 密码（注意，某些邮箱需要为SMTP服务单独设置授权码，详情查看相关帮助）EXWLACKYKALEWMBR
pass = EXWLACKYKALEWMBR
     */

}
