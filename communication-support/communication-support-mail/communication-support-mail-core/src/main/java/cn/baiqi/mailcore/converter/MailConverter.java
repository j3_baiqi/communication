package cn.baiqi.mailcore.converter;

import cn.baiqi.mailapi.dto.BatchSendMailInfoDTO;
import cn.baiqi.mailapi.dto.SendMailInfoDTO;
import cn.baiqi.mailcore.po.MailSendLog;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.converter
 * @createTime 2021/11/25 - 10:11
 * @description
 */
@Mapper(componentModel = "spring", typeConversionPolicy = ReportingPolicy.ERROR)//交给spring管理
public interface MailConverter {

    MailSendLog converter(SendMailInfoDTO sendMailInfoDTO);

    BatchSendMailInfoDTO converterBatch(SendMailInfoDTO sendMailInfoDTO);

    MailSendLog converter(BatchSendMailInfoDTO batchSendMailInfoDTO);
}
