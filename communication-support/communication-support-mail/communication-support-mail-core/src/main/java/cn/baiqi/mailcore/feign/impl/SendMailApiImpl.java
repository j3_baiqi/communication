package cn.baiqi.mailcore.feign.impl;

import cn.baiqi.common.constant.SendMailConstant;
import cn.baiqi.common.utils.AsynchronousExecuteUtil;
import cn.baiqi.mailapi.dto.BatchSendMailInfoDTO;
import cn.baiqi.mailapi.dto.SendMailInfoDTO;
import cn.baiqi.mailapi.v1.SendMailApi;
import cn.baiqi.mailcore.config.MailProperties;
import cn.baiqi.mailcore.converter.MailConverter;
import cn.baiqi.mailcore.po.MailSendLog;
import cn.baiqi.mailcore.service.MailSendLogService;
import cn.hutool.extra.mail.MailUtil;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author J3-白起
 * @package cn.baiqi.mailcore.feign.impl
 * @createTime 2021/11/26 - 12:21
 * @description
 */
@Slf4j
@Data
@RestController
@RequiredArgsConstructor
public class SendMailApiImpl implements SendMailApi {

    private final MailSendLogService mailSendLogService;
    private final MailConverter converter;
    private final MailProperties mailProperties;


    @Override
    public Boolean sendMail(SendMailInfoDTO sendMailInfoDTO) {
        var mailSendLog = converter.converter(sendMailInfoDTO);
        var batchSendMailInfoDTO = converter.converterBatch(sendMailInfoDTO);
        batchSendMailInfoDTO.setAddressList(Collections.singletonList(sendMailInfoDTO.getAddress()));
        // 重试 X 次发送
        var success = retryDoSendMail(batchSendMailInfoDTO, SendMailConstant.RETRY_NUMBER);
        mailSendLog.setSendTime(LocalDateTime.now());
        mailSendLog.setSuccess(success);
        List<MailSendLog> mailSendLogList = Collections.singletonList(mailSendLog);
        // 记录发送log
        AsynchronousExecuteUtil.execute(() -> {
            mailSendLogService.insertList(mailSendLogList);
        });
        return true;
    }

    @Override
    public Boolean sendMail(BatchSendMailInfoDTO batchSendMailInfoDTO) {
        // 重试 X 次发送
        var success = retryDoSendMail(batchSendMailInfoDTO, SendMailConstant.RETRY_NUMBER);
        List<MailSendLog> mailSendLogList = new ArrayList<>(batchSendMailInfoDTO.getAddressList().size());
        for (String address : batchSendMailInfoDTO.getAddressList()) {
            var mailSendLog = converter.converter(batchSendMailInfoDTO);
            mailSendLog.setAddress(address);
            mailSendLog.setSendTime(LocalDateTime.now());
            mailSendLog.setSuccess(success);
            mailSendLogList.add(mailSendLog);
        }
        // 记录发送log
        AsynchronousExecuteUtil.execute(() -> {
            mailSendLogService.insertList(mailSendLogList);
        });
        return true;
    }


    /**
     * 带失败重试的邮件发送
     *
     * @param batchSendMailInfoDTO
     * @param number               失败重试次数
     * @return
     */
    private Boolean retryDoSendMail(BatchSendMailInfoDTO batchSendMailInfoDTO, Integer number) {
        AtomicInteger atomicInteger = new AtomicInteger(number);
        do {
            if (atomicInteger.getAndDecrement() <= 0) {
                break;
            }
        } while (!doSendMail(batchSendMailInfoDTO));
        return atomicInteger.get() >= 0;
    }

    private Boolean doSendMail(BatchSendMailInfoDTO batchSendMailInfoDTO) {
        try {
            MailUtil.send(
                    mailProperties.mailAccount,
                    batchSendMailInfoDTO.getAddressList(),
                    batchSendMailInfoDTO.getTitle(),
                    batchSendMailInfoDTO.getContent(),
                    batchSendMailInfoDTO.getIsHtml()
            );
        } catch (Exception e) {
            //错误处理
            // throw new SysException("发送邮件失败！", e);
            log.error("发送邮件失败！，{}", e);
            return false;
        }
        return true;
    }


}

