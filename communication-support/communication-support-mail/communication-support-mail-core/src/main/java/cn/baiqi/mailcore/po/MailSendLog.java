package cn.baiqi.mailcore.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @TableName comm_mail_send_log
 */
@TableName(value ="comm_mail_send_log")
@Data
public class MailSendLog implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 发送地址
     */
    private String address;

    /**
     * 标题
     */
    private String title;


    /**
     * 模板id
     */
    private Long messageTempId;

    /**
     * 内容
     */
    private String content;

    /**
     * 发送时间
     */
    private LocalDateTime sendTime;

    /**
     * 0:失败，1：成功
     */
    @TableField(value = "is_seccess")
    private Boolean success;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}