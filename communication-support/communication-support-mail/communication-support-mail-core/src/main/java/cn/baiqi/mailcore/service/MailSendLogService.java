package cn.baiqi.mailcore.service;

import cn.baiqi.mailcore.po.MailSendLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *
 */
public interface MailSendLogService extends IService<MailSendLog> {

    /**
     * 插入list
     * @param mailSendLogList
     */
    void insertList(List<MailSendLog> mailSendLogList);
}
