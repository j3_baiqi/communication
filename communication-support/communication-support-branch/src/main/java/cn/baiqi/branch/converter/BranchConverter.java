package cn.baiqi.branch.converter;

import cn.baiqi.branch.api.v1.form.MessageTempForm;
import cn.baiqi.branch.api.v1.vo.MessageTempVO;
import cn.baiqi.branch.bo.MessageTempBO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.converter
 * @createTime 2021/11/25 - 10:11
 * @description
 */
@Mapper(componentModel = "spring", typeConversionPolicy = ReportingPolicy.ERROR)//交给spring管理
public interface BranchConverter {
    MessageTempVO converter(MessageTempBO messageTempBO);

    MessageTempBO converter(MessageTempForm messageTempForm);
}
