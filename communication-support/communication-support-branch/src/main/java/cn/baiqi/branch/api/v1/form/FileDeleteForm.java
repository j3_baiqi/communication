package cn.baiqi.branch.api.v1.form;

import lombok.Data;

import java.util.List;

/**
 * @author J3-白起
 * @package cn.baiqi.branch.api.v1.form
 * @createTime 2021/12/15 - 23:46
 * @description
 */
@Data
public class FileDeleteForm {

    private Integer type;
    List<String> urlList;

}
