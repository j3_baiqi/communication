package cn.baiqi.branch.service;

import cn.baiqi.branch.api.v1.query.MessageTempQuery;
import cn.baiqi.branch.bo.MessageTempBO;
import cn.baiqi.branch.po.MessageTemp;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【comm_message_temp】的数据库操作Service
* @createDate 2021-11-28 17:01:56
*/
public interface MessageTempService extends IService<MessageTemp> {

    IPage<MessageTempBO> getPage(Page<MessageTemp> page, MessageTempQuery messageTempQuery);
}
