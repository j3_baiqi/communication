package cn.baiqi.branch.bo;

import cn.baiqi.branch.po.MessageTemp;
import lombok.Data;

/**
 * @author J3-白起
 * @package cn.baiqi.branch.bo
 * @createTime 2021/11/28 - 18:00
 * @description
 */
@Data
public class MessageTempBO extends MessageTemp {
}
