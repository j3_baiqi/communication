package cn.baiqi.branch.api.v1.controller;

import cn.baiqi.branch.api.v1.form.MessageTempForm;
import cn.baiqi.branch.api.v1.query.MessageTempQuery;
import cn.baiqi.branch.api.v1.vo.MessageTempVO;
import cn.baiqi.branch.converter.BranchConverter;
import cn.baiqi.branch.po.MessageTemp;
import cn.baiqi.branch.service.MessageTempService;
import cn.baiqi.common.annotation.ResponseResult;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author J3-白起
 * @package cn.baiqi.branch.api.v1.controller
 * @createTime 2021/11/28 - 17:02
 * @description 消息模板
 */
@Slf4j
@AllArgsConstructor
@ResponseResult
@RequestMapping("/v1/messageTemp")
public class MessageTempController {

    private final MessageTempService messageTempService;
    private final BranchConverter converter;

    @GetMapping("")
    public IPage<MessageTempVO> getPage(Page<MessageTemp> page, MessageTempQuery messageTempQuery) {
        var iPage = messageTempService.getPage(page, messageTempQuery);
        return iPage.convert(converter::converter);
    }

    @PostMapping("")
    public void save(@RequestBody @Validated MessageTempForm messageTempForm) {
        messageTempService.save(converter.converter(messageTempForm));
    }

    @PostMapping("/update")
    public void update(@RequestBody @Validated MessageTempForm messageTempForm) {
        messageTempService.updateById(messageTempForm);
    }

    @GetMapping("/delete")
    public void delete(@RequestParam(name = "id") Long id) {
        messageTempService.removeById(id);
    }

}
