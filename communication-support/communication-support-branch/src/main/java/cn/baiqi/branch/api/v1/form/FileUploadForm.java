package cn.baiqi.branch.api.v1.form;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author J3-白起
 * @package cn.baiqi.branch.api.v1.form
 * @createTime 2021/12/15 - 23:40
 * @description
 */
@Data
public class FileUploadForm {
    private Integer type;
    private MultipartFile file;
    private MultipartFile[] files;
}
