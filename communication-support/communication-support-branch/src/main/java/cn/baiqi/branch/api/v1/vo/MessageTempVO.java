package cn.baiqi.branch.api.v1.vo;

import cn.baiqi.branch.po.MessageTemp;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author J3-白起
 * @package cn.baiqi.branch.api.v1.vo
 * @createTime 2021/11/28 - 17:05
 * @description
 */
@Accessors(chain = true)
@Data
public class MessageTempVO extends MessageTemp {
}
