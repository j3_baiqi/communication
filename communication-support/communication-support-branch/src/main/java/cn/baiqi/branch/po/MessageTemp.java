package cn.baiqi.branch.po;

import cn.baiqi.common.enums.MessageTempEnum;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @TableName comm_message_temp
 */
@Accessors(chain = true)
@TableName(value = "comm_message_temp")
@Data
public class MessageTemp implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 模板类型
     */
    private MessageTempEnum type;

    /**
     * 标题
     */
    private String title;

    /**
     * 模板内容
     */
    private String content;

    /**
     * 0:无效，1:有效
     */
    @TableField(value = "is_effective")
    private Boolean effective;

    /**
     * 创建事件
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改事件
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}