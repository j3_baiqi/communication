package cn.baiqi.branch.mapper;

import cn.baiqi.branch.api.v1.query.MessageTempQuery;
import cn.baiqi.branch.bo.MessageTempBO;
import cn.baiqi.branch.po.MessageTemp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
* @author Administrator
* @description 针对表【comm_message_temp】的数据库操作Mapper
* @createDate 2021-11-28 17:01:56
* @Entity cn.baiqi.branch.po.MessageTemp
*/
public interface MessageTempMapper extends BaseMapper<MessageTemp> {

    IPage<MessageTempBO> getPage(@Param("page") Page<MessageTemp> page, @Param("query") MessageTempQuery messageTempQuery);
}




