package cn.baiqi.branch.api.v1.form;

import cn.baiqi.branch.po.MessageTemp;
import cn.baiqi.common.enums.MessageTempEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;

/**
 * @author J3-白起
 * @package cn.baiqi.branch.api.v1.form
 * @createTime 2021/11/28 - 17:10
 * @description
 */
@Accessors(chain = true)
@Data
public class MessageTempForm extends MessageTemp {

    @NotEmpty(message = "模板内容不能为空")
    private String content;

}
