package cn.baiqi.branch.api.v1.query;

import lombok.Data;

/**
 * @author J3-白起
 * @package cn.baiqi.branch.api.v1.query
 * @createTime 2021/11/28 - 17:08
 * @description
 */
@Data
public class MessageTempQuery {
}
