package cn.baiqi.branch.api.v1.controller;

import cn.baiqi.branch.api.v1.form.FileDeleteForm;
import cn.baiqi.branch.api.v1.form.FileUploadForm;
import cn.baiqi.common.annotation.ResponseResult;
import cn.baiqi.common.utils.LinuxFileTemplate;
import cn.baiqi.common.utils.MyOssTemplate;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * @author J3-白起
 * @package cn.baiqi.socialcore.api.v1.controller
 * @createTime 2021/12/14 - 11:24
 * @description
 */
@Slf4j
@AllArgsConstructor
@ResponseResult
@RequestMapping("/v1/file")
public class FileUploadController {

    private final MyOssTemplate myOssTemplate;
    private final LinuxFileTemplate linuxFileTemplate;

    @PostMapping("/upload")
    public String upload(FileUploadForm form) {
        if (Objects.nonNull(form.getType()) && form.getType() == 1) {
            return myOssTemplate.upload(form.getFile());
        }
        return linuxFileTemplate.upload(form.getFile());
    }

    @PostMapping("/batch")
    public List<String> uploadBatch(FileUploadForm form) {
        if (Objects.nonNull(form.getType()) && form.getType() == 1) {
            return myOssTemplate.upload(form.getFiles());
        }
        return linuxFileTemplate.upload(form.getFiles());
    }

    @GetMapping("/delete")
    public void delete(@RequestParam(name = "url") String url, @RequestParam(name = "type", defaultValue = "2") Integer type) {
        if (type == 1) {
            myOssTemplate.delete(url);
            return;
        }
        linuxFileTemplate.delete(url);
    }

    @PostMapping("/deletes")
    public void deletes(@RequestBody FileDeleteForm form) {
        if (Objects.nonNull(form.getType()) && form.getType() == 1) {
            myOssTemplate.delete(form.getUrlList());
            return;
        }
        linuxFileTemplate.delete(form.getUrlList());
    }

}
