package cn.baiqi.branch.service.impl;

import cn.baiqi.branch.api.v1.query.MessageTempQuery;
import cn.baiqi.branch.bo.MessageTempBO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.baiqi.branch.po.MessageTemp;
import cn.baiqi.branch.service.MessageTempService;
import cn.baiqi.branch.mapper.MessageTempMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【comm_message_temp】的数据库操作Service实现
* @createDate 2021-11-28 17:01:56
*/
@Slf4j
@Data
@Service
@AllArgsConstructor
public class MessageTempServiceImpl extends ServiceImpl<MessageTempMapper, MessageTemp>
    implements MessageTempService{

    private final MessageTempMapper messageTempMapper;

    @Override
    public IPage<MessageTempBO> getPage(Page<MessageTemp> page, MessageTempQuery messageTempQuery) {
        return messageTempMapper.getPage(page, messageTempQuery);
    }
}




